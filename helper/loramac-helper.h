
#ifndef LORAMAC_HELPER_H
#define LORAMAC_HELPER_H

#include "ns3/net-device-container.h"
#include "ns3/object-factory.h"
#include "ns3/node-container.h"
#include "ns3/random-variable-stream.h"
#include "ns3/attribute.h"
#include "ns3/mobility-model.h"

#include "ns3/loramac-end-device.h"
#include "ns3/lorawan-mac.h"
#include "ns3/lora-net-device.h"
#include "ns3/lora-channel.h"
#include "ns3/end-device-lora-phy.h"
#include "ns3/lora-device-address-generator.h"

#include <vector>

namespace ns3{
namespace lorawan{
/**
 * \ingroup lorawan
 * \brief Builds mac layer for LoRaMac nodes.
 * 
 */
class LoRaMacHelper
{
public:
  LoRaMacHelper ();
  virtual ~LoRaMacHelper ();

  /////////////////////////////
  //Typedefs and Enumerations//
  /////////////////////////////
  /**
   * Define the operational region.
   */
  enum Regions { EU, US, China, EU433MHz, Australia, CN, AS923MHz, SouthKorea, ALOHA };

  /**
   * Set an attribute of the underlying MAC object.
   *
   * \param name the name of the attribute to set.
   * \param v the value of the attribute.
   */
  void Set (std::string name, const AttributeValue &v);

  /**
   * Set the address generator to use for creation of these nodes.
   */
  void SetAddressGenerator (Ptr<LoraDeviceAddressGenerator> addrGen);


  /**
   * @brief Set the Device Class Type (Class A | C)
   * 
   * @param type Class type enumeration(CLASS_A)
   */
  void SetDeviceClass(LoRaMacEndDevice::class_t type);

  /**
   * Set the region in which the device is to operate.
   */
  void SetRegion (Regions region);

  /**
   * Create the LorawanMac instance and connect it to a device
   *
   * \param node the node on which we wish to create a wifi MAC.
   * \param device the device within which this MAC will be created.
   * \returns a newly-created LorawanMac object.
   */
  Ptr<LorawanMac> Create (Ptr<Node> node, Ptr<NetDevice> device) const;

  /**
  * Set up the end device's data rates
   * This function assumes we are using the following convention:
   * SF7 -> DR5
   * SF8 -> DR4
   * SF9 -> DR3
   * SF10 -> DR2
   * SF11 -> DR1
   * SF12 -> DR0
   */
  static std::vector<int> SetSpreadingFactorsUp (NodeContainer endDevices, NodeContainer gateways,
                                                 Ptr<LoraChannel> channel);

  /**
   * Set up the end device's data rates according to the given distribution.
   */
  static std::vector<int> SetSpreadingFactorsGivenDistribution (NodeContainer endDevices,
                                                                NodeContainer gateways,
                                                                std::vector<double> distribution);

private:
  /**
   * Perform region-specific configurations for the 868 MHz EU band.
   */
  void ConfigureForEuRegion (Ptr<LoRaMacEndDevice> edMac) const;

  /**
   * Perform region-specific configurations for the ALOHA band.
   */
  void ConfigureForAlohaRegion (Ptr<LoRaMacEndDevice> edMac) const;

  /**
   * Apply configurations that are common both for the GatewayLorawanMac and the
   * ClassAEndDeviceLorawanMac classes.
   */
  void ApplyCommonEuConfigurations (Ptr<LorawanMac> lorawanMac) const;

  /**
   * Apply configurations that are common both for the GatewayLorawanMac and the
   * ClassAEndDeviceLorawanMac classes.
   */
  void ApplyCommonAlohaConfigurations (Ptr<LorawanMac> lorawanMac) const;

  ObjectFactory m_mac;

  Regions m_region;

  Ptr<LoraDeviceAddressGenerator> m_addrGen;

  LoRaMacEndDevice::class_t m_classType;
};
}// namespace lorawan

}// namespace ns3

#endif /* LORAMAC_HELPER */