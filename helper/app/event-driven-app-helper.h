

#ifndef EVENT_DRIVEN_APP_HELPER_H
#define EVENT_DRIVEN_APP_HELPER_H

#include "ns3/object-factory.h"
#include "ns3/address.h"
#include "ns3/attribute.h"
#include "ns3/net-device.h"
#include "ns3/node-container.h"
#include "ns3/application-container.h"
#include "ns3/event-driven-app.h"
#include <stdint.h>
#include <string>

namespace ns3 {
namespace lorawan {

/**
 * This class can be used to install PeriodicSender applications on a wide
 * range of nodes.
 */
class EventDrivenAppHelper
{
public:
  EventDrivenAppHelper ();

  ~EventDrivenAppHelper ();

  void SetAttribute (std::string name, const AttributeValue &value);

  ApplicationContainer Install (NodeContainer c) const;

  ApplicationContainer Install (Ptr<Node> node) const;

  /**
   * Set the period to be used by the applications created by this helper.
   *
   * A value of Seconds (0) results in randomly generated periods according to
   * the model contained in the TR 45.820 document.
   *
   * \param period The period to set
   */
  void SetPeriod (Time period);

  //void SetPacketSizeRandomVariable (Ptr <RandomVariableStream> rv);

  void SetPacketSize (uint8_t size);


private:
  Ptr<Application> InstallPriv (Ptr<Node> node) const;

  ObjectFactory m_factory;

  Ptr<UniformRandomVariable> m_initialDelay;

  Ptr<UniformRandomVariable> m_intervalDelayRV;

  Time m_period; //!< The period with which the application will be set to send
                 // messages

  //Ptr<RandomVariableStream> m_pktSizeRV; // whether or not a random component is added to the packet size

  uint8_t m_pktSize; // the packet size.

};

} // namespace ns3

}
#endif /* EVENT_DRIVEN_APP_HELPER_H */
