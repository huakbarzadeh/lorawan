

#include "ns3/loramac-radio-energy-model-helper.h"
#include "ns3/end-device-lora-phy.h"
#include "ns3/loramac-tx-current-model.h"

namespace ns3 {
namespace lorawan {

NS_LOG_COMPONENT_DEFINE("LoRaMacRadioEnergyModelHelper");

LoRaMacRadioEnergyModelHelper::LoRaMacRadioEnergyModelHelper ()
{
    m_radioEnergy.SetTypeId("ns3::LoRaMacRadioEnergyModel");
    m_depletionCallback.Nullify ();
    m_rechargedCallback.Nullify ();
}

LoRaMacRadioEnergyModelHelper::~LoRaMacRadioEnergyModelHelper ()
{
}

void
LoRaMacRadioEnergyModelHelper::Set (std::string name, const AttributeValue &v)
{
  m_radioEnergy.Set (name, v);
}

void 
LoRaMacRadioEnergyModelHelper::SetDepletionCallback (
                    LoRaMacRadioEnergyModel::LoRaMacRadioEnergyDepletionCallback callback)
{
    m_depletionCallback = callback;
}

void
LoRaMacRadioEnergyModelHelper::SetRechargedCallback (
                    LoRaMacRadioEnergyModel::LoRaMacRadioEnergyRechargedCallback callback)
{
    m_rechargedCallback = callback;
}

void 
LoRaMacRadioEnergyModelHelper::SetTxCurrentModel (std::string name,
                          std::string n0 , const AttributeValue &v0,
                          std::string n1 , const AttributeValue &v1,
                          std::string n2 , const AttributeValue &v2,
                          std::string n3 , const AttributeValue &v3,
                          std::string n4 , const AttributeValue &v4,
                          std::string n5 , const AttributeValue &v5,
                          std::string n6 , const AttributeValue &v6,
                          std::string n7 , const AttributeValue &v7,
                          std::string n8 , const AttributeValue &v8,
                          std::string n9 , const AttributeValue &v9,
                          std::string n10 , const AttributeValue &v10)
{
    ObjectFactory factory;
    factory.SetTypeId (name);
    factory.Set (n0, v0);
    factory.Set (n1, v1);
    factory.Set (n2, v2);
    factory.Set (n3, v3);
    factory.Set (n4, v4);
    factory.Set (n5, v5);
    factory.Set (n6, v6);
    factory.Set (n7, v7);
    factory.Set (n8, v8);
    factory.Set (n9, v9);
    factory.Set (n10, v10);
}

void 
LoRaMacRadioEnergyModelHelper::TracedAverageCurrentHandler (double txPowerDbm, uint8_t dataRate, 
                                        uint8_t classType, Time periodTime)
{
  std::cout << txPowerDbm <<", " <<dataRate <<", "
          <<classType<<", "<<periodTime.GetSeconds ()<<std::endl;
}

Ptr<DeviceEnergyModel>
LoRaMacRadioEnergyModelHelper::DoInstall(Ptr<NetDevice> device,
                                                Ptr<EnergySource> source) const
{
    NS_ASSERT (device != NULL);
    NS_ASSERT (source != NULL);
    // check if device is LoraNetDevice
    std::string deviceName = device->GetInstanceTypeId ().GetName ();
    if (deviceName.compare ("ns3::LoraNetDevice") != 0)
      {
        NS_FATAL_ERROR ("NetDevice type is not LoraNetDevice!");
      }
    Ptr<Node> node = device->GetNode ();
    Ptr<LoRaMacRadioEnergyModel> model = m_radioEnergy.Create ()->GetObject<LoRaMacRadioEnergyModel> ();
    NS_ASSERT (model != NULL);
    // set energy source pointer
    model->SetEnergySource (source);

    model->SetLoraNetDevice (device->GetObject<LoraNetDevice> ());

    // set energy depletion callback
    // if none is specified, make a callback to EndDeviceLoraPhy::SetSleepMode
    Ptr<LoraNetDevice> loraDevice = device->GetObject<LoraNetDevice> ();
    Ptr<EndDeviceLoraPhy> loraPhy = loraDevice->GetPhy ()->GetObject<EndDeviceLoraPhy> ();
    // add model to device model list in energy source
    source->AppendDeviceEnergyModel (model);
    // create and register energy model phy listener
    loraPhy->RegisterListener (model->GetPhyListener ());

    // model->TraceConnectWithoutContext ("AverageCurrentConsumption",MakeCallback(
    //                     &LoRaMacRadioEnergyModelHelper::TracedAverageCurrentHandler, this));

    if (m_txCurrentModel.GetTypeId ().GetUid ())
      {
        Ptr<LoRaMacTxCurrentModel> txcurrent = m_txCurrentModel.Create<LoRaMacTxCurrentModel> ();
        txcurrent->SetTxPowerCurrentTable(std::vector<double>{139, 103, 84, 76, 62, 57, 53, 51, 46});
        model->SetTxCurrentModel (txcurrent);
      }
    return model;
}





} // namespace lorawan
} // namespace ns3