

#include "ns3/battery-source-model-helper.h"
#include "ns3/battery-source-model.h"
#include "ns3/energy-source.h"

namespace ns3 {

BatterySourceModelHelper::BatterySourceModelHelper ()
{
  m_batterySourceModel.SetTypeId ("ns3::BatterySourceModel");
}

BatterySourceModelHelper::~BatterySourceModelHelper ()
{
}

void
BatterySourceModelHelper::Set (std::string name, const AttributeValue &v)
{
  m_batterySourceModel.Set (name, v);
}

Ptr<EnergySource>
BatterySourceModelHelper::DoInstall (Ptr<Node> node) const
{
  NS_ASSERT (node != NULL);
  Ptr<EnergySource> source = m_batterySourceModel.Create<EnergySource> ();
  NS_ASSERT (source != NULL);
  source->SetNode (node);
  return source;
}

} // namespace ns3