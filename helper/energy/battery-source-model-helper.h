

#ifndef BATTERY_SOURCE_MODEL_HELPER_H
#define BATTERY_SOURCE_MODEL_HELPER_H

#include "ns3/energy-model-helper.h"
#include "ns3/node.h"

namespace ns3 {

/**
 * \ingroup energy
 * \brief Creates a BasicEnergySource object.
 *
 */
class BatterySourceModelHelper : public EnergySourceHelper
{
public:
  BatterySourceModelHelper ();
  virtual ~BatterySourceModelHelper ();

  void Set (std::string name, const AttributeValue &v);

private:
  virtual Ptr<EnergySource> DoInstall (Ptr<Node> node) const;

private:
  ObjectFactory m_batterySourceModel;

};

} // namespace ns3

#endif  /* BATTERY_SOURCE_MODEL_HELPER_H */