

#ifndef LORAMAC_RADIO_ENERGY_MODEL_HELPER_H
#define LORAMAC_RADIO_ENERGY_MODEL_HELPER_H

#include "ns3/energy-model-helper.h"
#include "ns3/loramac-radio-energy-model.h"
#include "ns3/log.h"
#include "ns3/lora-net-device.h"

namespace ns3 {
namespace lorawan {

class LoRaMacRadioEnergyModelHelper: public DeviceEnergyModelHelper
{
public:
    /**
     * @brief Construct a new LoRaMac Radio Energy Model Helper object
     * 
     */
    LoRaMacRadioEnergyModelHelper ();

    /**
     * @brief Destroy the LoRaMac Radio Energy Model Helper object
     * 
     */
    virtual ~LoRaMacRadioEnergyModelHelper ();

    /**
     * @brief Sets an attribute of the underlying PHY object.
     * 
     * @param name the name of the attribute to set
     * @param v the value of the attribute
     */
    void Set (std::string name, const AttributeValue &v);

    /**
     * \param callback Callback function for energy depletion handling.
     *
     * Sets the callback to be invoked when energy is depleted.
     */
    void SetDepletionCallback (
        LoRaMacRadioEnergyModel::LoRaMacRadioEnergyDepletionCallback callback);

    /**
     * \param callback Callback function for energy recharged handling.
     *
     * Sets the callback to be invoked when energy is recharged.
     */
    void SetRechargedCallback (
        LoRaMacRadioEnergyModel::LoRaMacRadioEnergyDepletionCallback callback);

    /**
     * @brief Traced is called
     * 
     * @param txPowerDbm 
     * @param dataRate 
     * @param classType 
     * @param periodTime 
     */
    void TracedAverageCurrentHandler (double txPowerDbm, uint8_t dataRate, 
                                        uint8_t classType, Time periodTime);

    /**
     * \param device Pointer to the NetDevice to install DeviceEnergyModel.
     * \param source Pointer to EnergySource to install.
     * \returns Ptr<DeviceEnergyModel>
     *
     * Implements DeviceEnergyModel::Install.
     */
    virtual Ptr<DeviceEnergyModel> DoInstall (Ptr<NetDevice> device,
                                                Ptr<EnergySource> source) const;

    /**
   * \param name the name of the model to set
   * \param n0 the name of the attribute to set
   * \param v0 the value of the attribute to set
   * \param n1 the name of the attribute to set
   * \param v1 the value of the attribute to set
   * \param n2 the name of the attribute to set
   * \param v2 the value of the attribute to set
   * \param n3 the name of the attribute to set
   * \param v3 the value of the attribute to set
   * \param n4 the name of the attribute to set
   * \param v4 the value of the attribute to set
   * \param n5 the name of the attribute to set
   * \param v5 the value of the attribute to set
   * \param n6 the name of the attribute to set
   * \param v6 the value of the attribute to set
   * \param n7 the name of the attribute to set
   * \param v7 the value of the attribute to set
   * \param n8 the name of the attribute to set
   * \param v8 the value of the attribute to set
   * \param n9 the name of the attribute to set
   * \param v9 the value of the attribute to set
   * \param n10 the name of the attribute to set
   * \param v10 the value of the attribute to set
   *
   * Configure a Tx Current model for this EnergySource.
   */
  void SetTxCurrentModel (std::string name,
                          std::string n0 = "", const AttributeValue &v0 = EmptyAttributeValue (),
                          std::string n1 = "", const AttributeValue &v1 = EmptyAttributeValue (),
                          std::string n2 = "", const AttributeValue &v2 = EmptyAttributeValue (),
                          std::string n3 = "", const AttributeValue &v3 = EmptyAttributeValue (),
                          std::string n4 = "", const AttributeValue &v4 = EmptyAttributeValue (),
                          std::string n5 = "", const AttributeValue &v5 = EmptyAttributeValue (),
                          std::string n6 = "", const AttributeValue &v6 = EmptyAttributeValue (),
                          std::string n7 = "", const AttributeValue &v7 = EmptyAttributeValue (),
                          std::string n8 = "", const AttributeValue &v8 = EmptyAttributeValue (),
                          std::string n9 = "", const AttributeValue &v9 = EmptyAttributeValue (),
                          std::string n10 = "", const AttributeValue &v10 = EmptyAttributeValue ());

    
private:
    ObjectFactory m_radioEnergy; // radio energy model
    LoRaMacRadioEnergyModel::LoRaMacRadioEnergyDepletionCallback m_depletionCallback;
    LoRaMacRadioEnergyModel::LoRaMacRadioEnergyRechargedCallback m_rechargedCallback;
    ObjectFactory m_txCurrentModel;

    //LoraNetDevice m_loraNetDevice;

};

} // namespace lorawan
} // namespace ns3

#endif /* LORAMAC_RADIO_ENERGY_MODEL_HELPER_H */