
#ifndef LORAMAC_END_DEVICE_H
#define LORAMAC_END_DEVICE_H

#include "ns3/object.h"
#include "ns3/log.h"
#include "ns3/end-device-lorawan-mac.h"
#include "ns3/end-device-lora-phy.h"
#include "ns3/lorawan-mac.h"
#include "ns3/lora-frame-header.h"
#include "ns3/lora-device-address.h"
#include <algorithm>


namespace ns3{
namespace lorawan{

class LoRaMacEndDevice: public EndDeviceLorawanMac
{
public:
    static TypeId GetTypeId(void);

    LoRaMacEndDevice();
    virtual ~LoRaMacEndDevice();

    //////////////////////////////
    // Enumerations and Typedefs//
    //////////////////////////////

    /*!
     * Devices Class enumeration
     * Class B is not implemented yet
     */
    enum class_t{
        CLASS_A=0,    
        CLASS_C=2
    };

    /////////////////////
    // Sending methods //
    /////////////////////

    /**
     * Add headers and send a packet with the sending function of the physical layer.
     *
     * \param packet the packet to send
     */
    virtual void SendToPhy (Ptr<Packet> packet);

    //////////////////////////
    //  Receiving methods   //
    //////////////////////////

    /**
     * Receive a packet.
     *
     * This method is typically registered as a callback in the underlying PHY
     * layer so that it's called when a packet is going up the stack.
     *
     * \param packet the received packet.
     */
    virtual void Receive (Ptr<Packet const> packet);

    virtual void FailedReception (Ptr<Packet const> packet);

    /**
     * Perform the actions that are required after a packet send.
     *
     * This function handles opening of the first receive window.
     */
    virtual void TxFinished (Ptr<const Packet> packet);

    /**
     * Perform operations needed to open the first receive window.
     */
    void OpenFirstReceiveWindow (void);

    /**
     * Perform operations needed to open the second receive window.
     */
    void OpenSecondReceiveWindow (void);

    /**
     * Perform operations needed to close the first receive window.
     */
    void CloseFirstReceiveWindow (void);

    /**
     * Perform operations needed to close the second receive window.
     */
    void CloseSecondReceiveWindow (void);

    /////////////////////////
    // Getters and Setters //
    /////////////////////////

    /**
     * Find the minimum waiting time before the next possible transmission based
     * on End Device's Class Type.
     *
     * \param waitingTime The minimum waiting time that has to be respected,
     * irrespective of the class (e.g., because of duty cycle limitations).
     */
    virtual Time GetNextClassTransmissionDelay (Time waitingTime);

    /**
     * Get the Data Rate that will be used in the first receive window.
     *
     * \return The Data Rate
     */
    uint8_t GetFirstReceiveWindowDataRate (void);

    /**
     * Set the Data Rate to be used in the second receive window.
     *
     * \param dataRate The Data Rate.
     */
    void SetSecondReceiveWindowDataRate (uint8_t dataRate);

    /**
     * Get the Data Rate that will be used in the second receive window.
     *
     * \return The Data Rate
     */
    uint8_t GetSecondReceiveWindowDataRate (void);

    /**
     * Set the frequency that will be used for the second receive window.
     *
     * \param frequencyMHz the Frequency.
     */
    void SetSecondReceiveWindowFrequency (double frequencyMHz);

    /**
     * Get the frequency that is used for the second receive window.
     *
     * @return The frequency, in MHz
     */
    double GetSecondReceiveWindowFrequency (void);

    /**
     * Sets device class type. It can be accessed throw ns3 configuration
     * 
     * \param type the class type to be set.
     */
    void SetDeviceClassType(enum class_t type);

    /**
     * @brief Get device class type
     * 
     * @return uint8_t it will return class type as a byte (0x00 is Class A)
     */
    uint8_t GetDeviceClassType(void);

    /**
     * @brief Set the Confirm of Device Class object.
     * 
     * @param conf bool value whether confirmed or not.
     */
    void SetConfirmDeviceClass(bool conf);

    /**
     * @brief Get the Confirm of Device Class object.
     * 
     * @return true Device Class has been confirmed.
     * @return false Device Class has not been confirmed.
     */
    bool GetConfirmDeviceClass(void);

    /////////////////////////
    // MAC command methods //
    /////////////////////////

    /**
     * Perform the actions that need to be taken when receiving a RxParamSetupReq
     * command based on the Device's Class Type.
     *
     * \param rxParamSetupReq The Parameter Setup Request, which contains:
     *                            - The offset to set.
     *                            - The data rate to use for the second receive window.
     *                            - The frequency to use for the second receive window.
     */
    virtual void OnRxClassParamSetupReq (Ptr<RxParamSetupReq> rxParamSetupReq);

    /**
     * @brief changes underlyting phy state after end of recieve.
     * 
     */
    void ChangePhyStateEnd (void);



private:

    /**
     * @brief Handle a ConfirmedDownlink Acknoledgment
     * 
     */
    void ConfirmedDownAck (void);

    /**
     * Device Class Type. 
     * So far there is only class A and C
     */
    class_t m_devClass;

    /**
     * @brief Boolean wether device class is
     *        confirmed or not.
     * 
     */
    bool m_devClassConf;

    /**
     * The interval between when a packet is done sending and when the first
     * receive window is opened.
     */
    Time m_receiveDelay1;

    /**
     * The interval between when a packet is done sending and when the second
     * receive window is opened.
     */
    Time m_receiveDelay2;

    /**
     * The event of the closing the first receive window.
     *
     * This Event will be canceled if there's a successful reception of a packet.
     */
    EventId m_closeFirstWindow;

    /**
     * The event of the closing the second receive window.
     *
     * This Event will be canceled if there's a successful reception of a packet.
     */
    EventId m_closeSecondWindow;

    /**
     * The event of the second receive window opening.
     *
     * This Event is used to cancel the second window in case the first one is
     * successful.
     */
    EventId m_secondReceiveWindow;

    /**
     * The frequency to listen on for the second receive window.
     */
    double m_secondReceiveWindowFrequency;

    /**
     * The Data Rate to listen for during the second downlink transmission.
     */
    uint8_t m_secondReceiveWindowDataRate;

    /**
     * The RX1DROffset parameter value
     */
    uint8_t m_rx1DrOffset;


    Ptr<LogicalLoraChannel> m_txChannel;
};

}// namespace lorawan
}// namespace ns3

#endif  /* LORAMAC_END_DEVICE */