

#ifndef LORAMAC_TX_CURRENT_MODEL_H
#define LORAMAC_TX_CURRENT_MODEL_H

#include "ns3/object.h"
#include "ns3/vector.h"
#include "ns3/log.h"
#include <map>
#include <vector>

namespace ns3 {
namespace lorawan {

/**
 * \ingroup energy
 *
 * \brief Model the transmit current as a function of the transmit power and
 * mode.
 */
class LoRaMacTxCurrentModel : public Object
{
public:
  static TypeId GetTypeId (void);

  LoRaMacTxCurrentModel ();
  virtual ~LoRaMacTxCurrentModel ();

  /**
   * Get the current for transmission at this power.
   *
   * \param txPowerDbm The nominal tx power in dBm
   * \returns The transmit current (in Ampere)
   */
  virtual double CalcTxCurrent (double txPowerDbm) const;

  /**
   * \param voltage (Volts)
   *
   * Set the supply voltage.
   */
  void SetVoltage (double voltage);

  /**
   * \return the supply voltage.
   */
  double GetVoltage (void) const;

  /**
   * @brief Set the TxPower current table.
   * 
   * @param table table to be inserted. order must be {14, 12, ..., 0}(in dBm).
   */
  void SetTxPowerCurrentTable(const std::vector<double> table);

private:
  double m_voltage;     //!< Voltage
  double m_idleCurrent;     //!< Standby current
  std::vector<double> m_txPowerVector;

  // txPower index mapping
  // i know its lame
  std::map<double, int> m_indexMapping = {
                        {16, 0},
                        {14, 1},
                        {12, 2},
                        {10, 3},
                        {8, 4},
                        {6, 5},
                        {4, 6},
                        {2, 7},
                        {0, 8},
                        {100, 100}};// just to confuse more
};

} // namespace lorawan

} // namespace ns3
#endif /* LORAMAC_TX_CURRENT_MODEL_H */