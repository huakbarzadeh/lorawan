

#include "ns3/loramac-radio-energy-model.h"
#include "ns3/battery-source-model.h"
#include "ns3/lora-device-address.h"
#include "ns3/loramac-end-device.h"
#include <iomanip>


namespace ns3 {
namespace lorawan {

NS_LOG_COMPONENT_DEFINE ("LoRaMacRadioEnergyModel");

NS_OBJECT_ENSURE_REGISTERED(LoRaMacRadioEnergyModel);

//////////////////////////////////////
//LoRaMacRadioEnergyModelPhyListener//
//////////////////////////////////////
LoRaMacRadioEnergyModelPhyListener::LoRaMacRadioEnergyModelPhyListener ()
{
  NS_LOG_FUNCTION (this);
  m_changeStateCallback.Nullify ();
  m_updateTxCurrentCallback.Nullify ();
}

LoRaMacRadioEnergyModelPhyListener::~LoRaMacRadioEnergyModelPhyListener ()
{
  NS_LOG_FUNCTION (this);
}

void
LoRaMacRadioEnergyModelPhyListener::SetChangeStateCallback (DeviceEnergyModel::ChangeStateCallback callback)
{
  NS_LOG_FUNCTION (this << &callback);
  NS_ASSERT (!callback.IsNull ());
  m_changeStateCallback = callback;
}

void
LoRaMacRadioEnergyModelPhyListener::SetUpdateTxCurrentCallback (UpdateTxCurrentCallback callback)
{
  NS_LOG_FUNCTION (this << &callback);
  NS_ASSERT (!callback.IsNull ());
  m_updateTxCurrentCallback = callback;
}

void
LoRaMacRadioEnergyModelPhyListener::NotifyRxStart ()
{
  NS_LOG_FUNCTION (this);
  if (m_changeStateCallback.IsNull ())
    {
      NS_FATAL_ERROR ("LoRaMacRadioEnergyModelPhyListener:Change state callback not set!");
    }
  m_changeStateCallback (EndDeviceLoraPhy::RX);
}

void
LoRaMacRadioEnergyModelPhyListener::NotifyTxStart (double txPowerDbm)
{
  NS_LOG_FUNCTION (this << txPowerDbm);
  //std::cout<<"dBm: "<<txPowerDbm<<std::endl;
  if (m_updateTxCurrentCallback.IsNull ())
    {
      NS_FATAL_ERROR ("LoRaMacRadioEnergyModelPhyListener:Update tx current callback not set!");
    }
  m_updateTxCurrentCallback (txPowerDbm);
  if (m_changeStateCallback.IsNull ())
    {
      NS_FATAL_ERROR ("LoRaMacRadioEnergyModelPhyListener:Change state callback not set!");
    }
  m_changeStateCallback (EndDeviceLoraPhy::TX);
}

void
LoRaMacRadioEnergyModelPhyListener::NotifySleep (void)
{
  NS_LOG_FUNCTION (this);
  if (m_changeStateCallback.IsNull ())
    {
      NS_FATAL_ERROR ("LoRaMacRadioEnergyModelPhyListener:Change state callback not set!");
    }
  m_changeStateCallback (EndDeviceLoraPhy::SLEEP);
}

void
LoRaMacRadioEnergyModelPhyListener::NotifyStandby (void)
{
  NS_LOG_FUNCTION (this);
  if (m_changeStateCallback.IsNull ())
    {
      NS_FATAL_ERROR ("LoRaMacRadioEnergyModelPhyListener:Change state callback not set!");
    }
  m_changeStateCallback (EndDeviceLoraPhy::STANDBY);
}

/*
 * Private function state here.
 */

void
LoRaMacRadioEnergyModelPhyListener::SwitchToStandby (void)
{
  NS_LOG_FUNCTION (this);
  if (m_changeStateCallback.IsNull ())
    {
      NS_FATAL_ERROR ("LoRaMacRadioEnergyModelPhyListener:Change state callback not set!");
    }
  m_changeStateCallback (EndDeviceLoraPhy::STANDBY);
}



///////////////////////////
//LoRaMacRadioEnergyModel//
///////////////////////////
TypeId
LoRaMacRadioEnergyModel::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::LoRaMacRadioEnergyModel")
    .SetParent<DeviceEnergyModel> ()
    .SetGroupName ("Energy")
    .AddConstructor<LoRaMacRadioEnergyModel> ()
    .AddAttribute ("StandbyCurrentA",
                   "The default radio Standby current in Ampere.",
                   DoubleValue (0.0016),      // idle mode = 1.6mA
                   MakeDoubleAccessor (&LoRaMacRadioEnergyModel::SetStandbyCurrentA,
                                       &LoRaMacRadioEnergyModel::GetStandbyCurrentA),
                   MakeDoubleChecker<double> ())
    .AddAttribute ("TxCurrentA",
                   "The radio Tx current in miliAmpere.",
                   DoubleValue (0.103),        // transmit at 14dBm = 103mA
                   MakeDoubleAccessor (&LoRaMacRadioEnergyModel::SetTxCurrentA,
                                       &LoRaMacRadioEnergyModel::GetTxCurrentA),
                   MakeDoubleChecker<double> ())
    .AddAttribute ("RxCurrentA",
                   "The radio Rx current in Ampere.",
                   DoubleValue (0.014),        // receive mode = 14mA
                   MakeDoubleAccessor (&LoRaMacRadioEnergyModel::SetRxCurrentA,
                                       &LoRaMacRadioEnergyModel::GetRxCurrentA),
                   MakeDoubleChecker<double> ())
    .AddAttribute ("SleepCurrentA",
                   "The radio Sleep current in Ampere.",
                   DoubleValue (0.0000015),      // sleep mode = 1.5microA
                   MakeDoubleAccessor (&LoRaMacRadioEnergyModel::SetSleepCurrentA,
                                       &LoRaMacRadioEnergyModel::GetSleepCurrentA),
                   MakeDoubleChecker<double> ())
    .AddAttribute ("TxCurrentModel", 
                   "A pointer to the attached tx current model.",
                   PointerValue (),
                   MakePointerAccessor (&LoRaMacRadioEnergyModel::m_txCurrentModel),
                   MakePointerChecker<LoRaMacTxCurrentModel> ())
    .AddTraceSource ("TotalEnergyConsumption",
                     "Total energy consumption of the radio device",
                     MakeTraceSourceAccessor (&LoRaMacRadioEnergyModel::m_totalEnergyConsumption),
                     "ns3::TracedValueCallback::Double")
    .AddTraceSource ("DrainedCapacity",
                     "Drained capacity for between two consequent transmission.",
                     MakeTraceSourceAccessor (&LoRaMacRadioEnergyModel::m_drainCapacity),
                     "ns3::TracesValueCallback::Double")
    .AddTraceSource ("TotalDrainedCapacity",
                     "Total capacity energy model has consumed.",
                     MakeTraceSourceAccessor (&LoRaMacRadioEnergyModel::m_totalDrainedCapacity),
                     "ns3::TracesValueCallback::Double")
    .AddTraceSource ("AverageCurrentConsumption",
                     "Average current consumed in send period.",
                     MakeTraceSourceAccessor (&LoRaMacRadioEnergyModel::m_averageCurrent),
                     "ns3::LoRaMacRadioEnergyModel::AverageCurrentConsumptionTracedCallback");
  return tid;
}

LoRaMacRadioEnergyModel::LoRaMacRadioEnergyModel ()
{
  NS_LOG_FUNCTION (this);
  m_currentState = EndDeviceLoraPhy::SLEEP;      // initially STANDBY
  m_lastUpdateTime = Seconds (0.0);
  m_timeInterval = Seconds (0.0);
  m_nPendingChangeState = 0;
  m_isSupersededChangeState = false;
  m_energyDepletionCallback.Nullify ();
  m_totalDrainedCapacity = 0.0;
  m_source = NULL;
  m_drainCapacity = 0.0;
  // set callback for EndDeviceLoraPhy listener
  m_listener = new LoRaMacRadioEnergyModelPhyListener;
  m_listener->SetChangeStateCallback (MakeCallback (&DeviceEnergyModel::ChangeState, this));
  // set callback for updating the tx current
  m_listener->SetUpdateTxCurrentCallback (MakeCallback (&LoRaMacRadioEnergyModel::SetTxCurrentFromModel, this));
}

LoRaMacRadioEnergyModel::~LoRaMacRadioEnergyModel ()
{
  NS_LOG_FUNCTION (this);
  delete m_listener;
}

void
LoRaMacRadioEnergyModel::SetEnergySource (Ptr<EnergySource> source)
{
  NS_LOG_FUNCTION (this << source);
  NS_ASSERT (source != NULL);
  m_source = source;
}

double
LoRaMacRadioEnergyModel::GetTotalEnergyConsumption (void) const
{
  NS_LOG_FUNCTION (this);
  return m_totalEnergyConsumption;
}

double
LoRaMacRadioEnergyModel::GetDrainedCapacity (void) const
{
  NS_LOG_FUNCTION (this);
  return m_drainCapacity;
}

double
LoRaMacRadioEnergyModel::GetStandbyCurrentA (void) const
{
  NS_LOG_FUNCTION (this);
  return m_idleCurrentA;
}

void
LoRaMacRadioEnergyModel::SetStandbyCurrentA (double idleCurrentA)
{
  NS_LOG_FUNCTION (this << idleCurrentA);
  m_idleCurrentA = idleCurrentA;
}

double
LoRaMacRadioEnergyModel::GetTxCurrentA (void) const
{
  NS_LOG_FUNCTION (this);
  return m_txCurrentA;
}

void
LoRaMacRadioEnergyModel::SetTxCurrentA (double txCurrentA)
{
  NS_LOG_FUNCTION (this << txCurrentA);
  m_txCurrentA = txCurrentA;
}

double
LoRaMacRadioEnergyModel::GetRxCurrentA (void) const
{
  NS_LOG_FUNCTION (this);
  return m_rxCurrentA;
}

void
LoRaMacRadioEnergyModel::SetRxCurrentA (double rxCurrentA)
{
  NS_LOG_FUNCTION (this << rxCurrentA);
  m_rxCurrentA = rxCurrentA;
}

double
LoRaMacRadioEnergyModel::GetSleepCurrentA (void) const
{
  NS_LOG_FUNCTION (this);
  return m_sleepCurrentA;
}

void
LoRaMacRadioEnergyModel::SetSleepCurrentA (double sleepCurrentA)
{
  NS_LOG_FUNCTION (this << sleepCurrentA);
  m_sleepCurrentA = sleepCurrentA;
}

EndDeviceLoraPhy::State
LoRaMacRadioEnergyModel::GetCurrentState (void) const
{
  NS_LOG_FUNCTION (this);
  return m_currentState;
}

void
LoRaMacRadioEnergyModel::SetEnergyDepletionCallback (
  LoRaMacRadioEnergyDepletionCallback callback)
{
  NS_LOG_FUNCTION (this);
  if (callback.IsNull ())
    {
      NS_LOG_DEBUG ("LoRaMacRadioEnergyModel:Setting NULL energy depletion callback!");
    }
  m_energyDepletionCallback = callback;
}

void
LoRaMacRadioEnergyModel::SetEnergyRechargedCallback (
  LoRaMacRadioEnergyRechargedCallback callback)
{
  NS_LOG_FUNCTION (this);
  if (callback.IsNull ())
    {
      NS_LOG_DEBUG ("LoRaMacRadioEnergyModel:Setting NULL energy recharged callback!");
    }
  m_energyRechargedCallback = callback;
}

void
LoRaMacRadioEnergyModel::SetTxCurrentModel (Ptr<LoRaMacTxCurrentModel> model)
{
  m_txCurrentModel = model;
}

void
LoRaMacRadioEnergyModel::SetTxCurrentFromModel (double txPowerDbm)
{
  if (m_txCurrentModel)
    {
      m_txCurrentA = m_txCurrentModel->CalcTxCurrent (txPowerDbm);
    }
}

void
LoRaMacRadioEnergyModel::ChangeState (int newState)
{
  NS_LOG_FUNCTION (this << newState);

  Time duration = Simulator::Now () - m_lastUpdateTime;
  NS_ASSERT (duration.GetNanoSeconds () >= 0);     // check if duration is valid

  // Ptr<LoRaMacEndDevice> mymac = m_loraNetDevice->GetMac ()->GetObject<LoRaMacEndDevice> ();
  // NS_LOG_INFO ("DataRate"<<mymac->GetDataRate ()<<"Device Add: "<<mymac->GetDeviceAddress ()<<
  //                 "SF "<<mymac->GetSfFromDataRate (mymac->GetDataRate ())<<"BW: "<<
  //                 mymac->GetBandwidthFromDataRate (mymac->GetDataRate ()));

  m_timeInterval += duration;

  // energy to decrease = current * voltage * time
  double drainedCapacity = 0.0;
  switch (m_currentState)
    {
    case EndDeviceLoraPhy::STANDBY:
      drainedCapacity = m_idleCurrentA * (duration.GetSeconds () / 3600);// *1000/36000
      break;
    case EndDeviceLoraPhy::TX:
      {
        Ptr<LoRaMacEndDevice> ptr = m_loraNetDevice->GetMac ()->GetObject<LoRaMacEndDevice> ();
        // m_averageCurrent (m_drainCapacity, ptr->GetDataRate (), ptr->GetDeviceClassType (), m_timeInterval);
        NS_LOG_DEBUG (std::setprecision(20)<<m_drainCapacity<<" "<<ptr->GetDataRate ()<<" "<<
                      ptr->GetDeviceClassType ()<<" "<<m_timeInterval.GetSeconds ());
        if (ptr->GetNewPacketInd ())
        {
          m_drainCapacity = 0.0; // refereshing drained capacity for each transmission
          m_timeInterval = Seconds(0.0);
        }
        drainedCapacity = m_txCurrentA * (duration.GetSeconds () / 3.600);// *1000/36000
        break;
      }
    case EndDeviceLoraPhy::RX:
      drainedCapacity = m_rxCurrentA * (duration.GetSeconds () / 3.600);// *1000/36000
      break;
    case EndDeviceLoraPhy::SLEEP:
      {
        drainedCapacity = m_sleepCurrentA * (duration.GetSeconds () / 3.600);// *1000/36000
        break;
      }
    default:
      NS_FATAL_ERROR ("LoRaMacRadioEnergyModel:Undefined radio state: " << m_currentState);
    }

  // update total energy consumption
  m_totalEnergyConsumption += drainedCapacity;

  m_totalDrainedCapacity += drainedCapacity;

  m_drainCapacity += drainedCapacity;

  // update last update time stamp
  m_lastUpdateTime = Simulator::Now ();

  m_nPendingChangeState++;

  // setting draied capacity for further calculations
  //m_source->GetObject<BatterySourceModel> ()-> SetDrainedCapacity (drainedCapacity);
  // notify energy source
  m_source->GetObject<BatterySourceModel> ()->UpdateEnergySource (drainedCapacity);

  // in case the energy source is found to be depleted during the last update, a callback might be
  // invoked that might cause a change in the Lora PHY state (e.g., the PHY is put into SLEEP mode).
  // This in turn causes a new call to this member function, with the consequence that the previous
  // instance is resumed after the termination of the new instance. In particular, the state set
  // by the previous instance is erroneously the final state stored in m_currentState. The check below
  // ensures that previous instances do not change m_currentState.

  if (!m_isSupersededChangeState)
    {
      // update current state & last update time stamp
      SetLoraRadioState ((EndDeviceLoraPhy::State) newState);

      // some debug message
      NS_LOG_DEBUG ("LoRaMacRadioEnergyModel:Total capacity consumption is " <<
                    m_totalEnergyConsumption/1000.0 << "mAh");
    }

  m_isSupersededChangeState = (m_nPendingChangeState > 1);

  m_nPendingChangeState--;
}

void
LoRaMacRadioEnergyModel::HandleEnergyDepletion (void)
{
  NS_LOG_FUNCTION (this);
  NS_LOG_DEBUG ("LoRaMacRadioEnergyModel:Energy is depleted!");
  // invoke energy depletion callback, if set.
  if (!m_energyDepletionCallback.IsNull ())
    {
      m_energyDepletionCallback ();
    }
}

void
LoRaMacRadioEnergyModel::HandleEnergyChanged (void)
{
  NS_LOG_FUNCTION (this);
  NS_LOG_DEBUG ("LoRaMacRadioEnergyModel:Energy changed!");
}

void
LoRaMacRadioEnergyModel::HandleEnergyRecharged (void)
{
  NS_LOG_FUNCTION (this);
  NS_LOG_DEBUG ("LoRaMacRadioEnergyModel:Energy is recharged!");
  // invoke energy recharged callback, if set.
  if (!m_energyRechargedCallback.IsNull ())
    {
      m_energyRechargedCallback ();
    }
}

LoRaMacRadioEnergyModelPhyListener *
LoRaMacRadioEnergyModel::GetPhyListener (void)
{
  NS_LOG_FUNCTION (this);
  return m_listener;
}

/*
 * Private functions start here.
 */

void
LoRaMacRadioEnergyModel::DoDispose (void)
{
  NS_LOG_FUNCTION (this);
  m_source = NULL;
  m_energyDepletionCallback.Nullify ();
}

double
LoRaMacRadioEnergyModel::DoGetCurrentA (void) const
{
  NS_LOG_FUNCTION (this);
  switch (m_currentState)
    {
    case EndDeviceLoraPhy::STANDBY:
      return m_idleCurrentA;
    case EndDeviceLoraPhy::TX:
      return m_txCurrentA;
    case EndDeviceLoraPhy::RX:
      return m_rxCurrentA;
    case EndDeviceLoraPhy::SLEEP:
      return m_sleepCurrentA;
    default:
      NS_FATAL_ERROR ("LoRaMacRadioEnergyModel:Undefined radio state:" << m_currentState);
    }
}

void
LoRaMacRadioEnergyModel::SetLoraRadioState (const EndDeviceLoraPhy::State state)
{
  NS_LOG_FUNCTION (this << state);
  m_currentState = state;
  std::string stateName;
  switch (state)
    {
    case EndDeviceLoraPhy::STANDBY:
      stateName = "STANDBY";
      break;
    case EndDeviceLoraPhy::TX:
      {
        Ptr<LoRaMacEndDevice> ptr = m_loraNetDevice->GetMac ()->GetObject<LoRaMacEndDevice> ();
        if (ptr->GetNewPacketInd ())
        {
          std::cout<<std::setprecision(20)<<ptr->GetDeviceAddress ()<<": "<<m_drainCapacity
                          <<" ";
          (int)ptr->GetDataRate ()==0?std::cout<<"0 ":std::cout<<(int)ptr->GetDataRate ()<<" ";
          (int)ptr->GetDeviceClassType ()==0?std::cout<<" Class_A":std::cout<<"Class_C";
          std::cout<<" "<<m_timeInterval.GetSeconds ()+1.0<<std::endl;
        }
        stateName = "TX";
        break;
      }
    case EndDeviceLoraPhy::RX:
      stateName = "RX";
      break;
    case EndDeviceLoraPhy::SLEEP:
      {
        stateName = "SLEEP";
        break;
      }
    }
  NS_LOG_DEBUG ("LoRaMacRadioEnergyModel:Switching to state: " << stateName <<
                " at time = " << Simulator::Now ().GetSeconds () << " s");
}

void
LoRaMacRadioEnergyModel::PrintTotalConsumption (void)
{
  NS_LOG_FUNCTION (this);

  Ptr<LoRaMacEndDevice> ptr = m_loraNetDevice->GetMac ()->GetObject<LoRaMacEndDevice> ();
  std::cout<<std::setprecision(20)<<ptr->GetDeviceAddress ()<<": "<<m_totalDrainedCapacity
                  <<" "<<(int)ptr->GetDataRate ()+1.0;
  (int)ptr->GetDeviceClassType ()==0?std::cout<<" Class_A":std::cout<<"Class_C";
  std::cout<<std::endl;
}

void 
LoRaMacRadioEnergyModel::SetLoraNetDevice (Ptr<LoraNetDevice> loraNet)
{
  m_loraNetDevice = loraNet;
}

} // namespace lorawan
} // namespace ns3
