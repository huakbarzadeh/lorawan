
#ifndef BATTERY_SOURCE_MODEL_H
#define BATTERY_SOURCE_MODEL_H

#include "ns3/traced-value.h"
#include "ns3/nstime.h"
#include "ns3/event-id.h"
#include "ns3/energy-source.h"

namespace ns3 {

/**
 * \ingroup energy
 * 
 * A basic battery source model implementation.
 * It is based on a linear batter model.
 *
 */
class BatterySourceModel : public EnergySource
{
public:
  static TypeId GetTypeId (void);
  BatterySourceModel ();
  virtual ~BatterySourceModel ();

  /**
   * \return Initial energy stored in energy source, in Joules.
   *
   * Implements GetInitialEnergy.
   */
  virtual double GetInitialEnergy (void) const;

  /**
   * @brief Get the Nominal Capacity of battery model in mAh.
   * 
   * @return double nominal capacity in mAh.
   */
  virtual double GetNominalCapacity (void) const;

  /**
   * \returns Supply voltage at the energy source.
   *
   * Implements GetSupplyVoltage.
   */
  virtual double GetSupplyVoltage (void) const;

  /**
   * \return Remaining energy in energy source, in Joules
   *
   * Implements GetRemainingEnergy.
   */
  virtual double GetRemainingEnergy (void);

  /**
   * @brief Get the Remaining Capacity of battery
   * 
   * @return double remaining capacity in mAh.
   */
  virtual double GetRemainingCapacity (void);

  /**
   * \returns Energy fraction.
   *
   * Implements GetEnergyFraction.
   */
  virtual double GetEnergyFraction (void);

  /**
   * Implements UpdateEnergySource.
   */
  virtual void UpdateEnergySource (void);
  
  /**
   * @brief Updates Energy Source capacity based on given depleted amount
   * 
   * @param drain depleted amount in mAh.
   */
  virtual void UpdateEnergySource (double drain);

  /**
   * \param initialEnergyJ Initial energy, in Joules
   *
   * Sets initial energy stored in the energy source. Note that initial energy
   * is assumed to be set before simulation starts and is set only once per
   * simulation.
   */
  void SetInitialEnergy (double initialEnergyJ);

  /**
   * @brief Set the Nominal Capacity of battery model
   * 
   * @param nominalCapmAh nominal capacity value in mAh.
   */
  void SetNominalCapacity (double nominalCapmAh);

  /**
   * @brief Set the drained capacity
   *        this function must be used befor UpdateEnergySource to 
   *        setup drained capacity.
   * 
   * @param drainedCapacity drained capacity in mAh.
   */
  void SetDrainedCapacity (double drainedCapacity);

  /**
   * \param supplyVoltageV Supply voltage at the energy source, in Volts.
   *
   * Sets supply voltage of the energy source.
   */
  void SetSupplyVoltage (double supplyVoltageV);

  /**
   * \param interval Energy update interval.
   *
   * This function sets the interval between each energy update.
   */
  void SetEnergyUpdateInterval (Time interval);

  /**
   * \returns The interval between each energy update.
   */
  Time GetEnergyUpdateInterval (void) const;


private:
  /// Defined in ns3::Object
  void DoInitialize (void);

  /// Defined in ns3::Object
  void DoDispose (void);

  /**
   * Handles the remaining energy going to zero event. This function notifies
   * all the energy models aggregated to the node about the energy being
   * depleted. Each energy model is then responsible for its own handler.
   */
  void HandleEnergyDrainedEvent (void);

  /**
   * Handles the remaining energy exceeding the high threshold after it went
   * below the low threshold. This function notifies all the energy models
   * aggregated to the node about the energy being recharged. Each energy model
   * is then responsible for its own handler.
   */
  void HandleEnergyRechargedEvent (void);

  /**
   * Calculates remaining energy. This function uses the total current from all
   * device models to calculate the amount of energy to decrease. The energy to
   * decrease is given by:
   *    energy to decrease = total current * supply voltage * time duration
   * This function subtracts the calculated energy to decrease from remaining
   * energy.
   */
  void CalculateRemainingEnergy (void);

  /**
   * @brief Calculates remaining battery capacity.
   * 
   */
  void CalculateRemainingCapacity (void);

private:
  double m_initialEnergyJ;                // initial energy, in Joules
  double m_nominalCapacity;                // nominal battery capacity, in mAh
  double m_supplyVoltageV;                // supply voltage, in Volts
  double m_lowBatteryTh;                  // low battery threshold, as a fraction of the initial energy
  double m_highBatteryTh;                 // high battery threshold, as a fraction of the initial energy
  double m_drainCapacity;                 // drained capacity in mAh
  bool m_depleted;                        // set to true when the remaining energy goes below the low threshold,
                                          // set to false again when the remaining energy exceeds the high threshold
  TracedValue<double> m_remainingEnergyJ; // remaining energy, in Joules
  TracedValue<double> m_remainingCapacity;// remaining battery capacity, in mAh
  EventId m_energyUpdateEvent;            // energy update event
  Time m_lastUpdateTime;                  // last update time
  Time m_energyUpdateInterval;            // energy update interval

};

} // namespace ns3

#endif /* BATTERY_SOURCE_MODEL_H */
