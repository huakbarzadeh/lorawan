


#include "ns3/loramac-tx-current-model.h"
#include "ns3/log.h"
#include "ns3/double.h"

namespace ns3 {
namespace lorawan {

NS_LOG_COMPONENT_DEFINE ("LoRaMacTxCurrentModel");

NS_OBJECT_ENSURE_REGISTERED (LoRaMacTxCurrentModel);

TypeId
LoRaMacTxCurrentModel::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::LoRaMacTxCurrentModel")
    .SetParent<Object> ()
    .SetGroupName ("lorawan")
    .AddConstructor<LoRaMacTxCurrentModel> ()
    .AddAttribute ("Voltage", "The supply voltage (in Volts).",
                   DoubleValue (3.3),
                   MakeDoubleAccessor (&LoRaMacTxCurrentModel::SetVoltage,
                                       &LoRaMacTxCurrentModel::GetVoltage),
                   MakeDoubleChecker<double> ());
                   
  return tid;
}

LoRaMacTxCurrentModel::LoRaMacTxCurrentModel (): m_txPowerVector(9,0)
{
    NS_LOG_FUNCTION (this);
}

LoRaMacTxCurrentModel::~LoRaMacTxCurrentModel ()
{
}

void 
LoRaMacTxCurrentModel::SetTxPowerCurrentTable(const std::vector<double> table)
{
  NS_LOG_FUNCTION(this);

  NS_ASSERT_MSG(table.size() == m_txPowerVector.size(), "Input Table does not match dimensions.");

  m_txPowerVector = table;
}

void
LoRaMacTxCurrentModel::SetVoltage (double voltage)
{
  NS_LOG_FUNCTION (this << voltage);
  m_voltage = voltage;
}

double
LoRaMacTxCurrentModel::GetVoltage (void) const
{
  return m_voltage;
}

double
LoRaMacTxCurrentModel::CalcTxCurrent (double txPowerDbm) const
{
  NS_LOG_FUNCTION (this << txPowerDbm);

  auto it = m_indexMapping.find(txPowerDbm);

  NS_ASSERT_MSG(it != m_indexMapping.cend(), "Requested txPowerDbm is unknown.");
  
  return m_txPowerVector.at(it->second);
}


} // namespace lorawan
} // namespace ns3