

#include "ns3/battery-source-model.h"
#include "ns3/log.h"
#include "ns3/assert.h"
#include "ns3/double.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/simulator.h"
#include <iomanip>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("BatterySourceModel");

NS_OBJECT_ENSURE_REGISTERED (BatterySourceModel);

TypeId
BatterySourceModel::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::BatterySourceModel")
    .SetParent<EnergySource> ()
    .SetGroupName ("Energy")
    .AddConstructor<BatterySourceModel> ()
    .AddAttribute ("BatterySourceModelInitialEnergyJ",
                   "Initial energy stored in basic energy source.",
                   DoubleValue (10),  // in Joules
                   MakeDoubleAccessor (&BatterySourceModel::SetInitialEnergy,
                                       &BatterySourceModel::GetInitialEnergy),
                   MakeDoubleChecker<double> ())
    .AddAttribute ("BatterySourceModelNominalCapacity",
                    "Nomincal Capacity of battery",
                    DoubleValue (2400), // in mAh 
                    MakeDoubleAccessor (&BatterySourceModel::SetNominalCapacity,
                                        &BatterySourceModel::GetNominalCapacity),
                    MakeDoubleChecker<double> ())
    .AddAttribute ("BasicEnergySupplyVoltageV",
                   "Initial supply voltage for basic energy source.",
                   DoubleValue (3.0), // in Volts
                   MakeDoubleAccessor (&BatterySourceModel::SetSupplyVoltage,
                                       &BatterySourceModel::GetSupplyVoltage),
                   MakeDoubleChecker<double> ())
    .AddAttribute ("BasicEnergyLowBatteryThreshold",
                   "Low battery threshold for basic energy source.",
                   DoubleValue (0.10), // as a fraction of the initial energy
                   MakeDoubleAccessor (&BatterySourceModel::m_lowBatteryTh),
                   MakeDoubleChecker<double> ())
    .AddAttribute ("BasicEnergyHighBatteryThreshold",
                   "High battery threshold for basic energy source.",
                   DoubleValue (0.15), // as a fraction of the initial energy
                   MakeDoubleAccessor (&BatterySourceModel::m_highBatteryTh),
                   MakeDoubleChecker<double> ())
    .AddAttribute ("PeriodicEnergyUpdateInterval",
                   "Time between two consecutive periodic energy updates.",
                   TimeValue (Seconds (1.0)),
                   MakeTimeAccessor (&BatterySourceModel::SetEnergyUpdateInterval,
                                     &BatterySourceModel::GetEnergyUpdateInterval),
                   MakeTimeChecker ())
    .AddTraceSource ("RemainingEnergy",
                     "Remaining energy at BatterySourceModel.",
                     MakeTraceSourceAccessor (&BatterySourceModel::m_remainingEnergyJ),
                     "ns3::TracedValueCallback::Double")
    .AddTraceSource ("RamainingCapacity",
                     "Ramaining capacity at BatterySourceModel.",
                     MakeTraceSourceAccessor (&BatterySourceModel::m_remainingCapacity),
                     "ns3::TracedValueCallback::Double")
  ;
  return tid;
}

BatterySourceModel::BatterySourceModel ()
{
  NS_LOG_FUNCTION (this);
  m_lastUpdateTime = Seconds (0.0);
  m_depleted = false;
}

BatterySourceModel::~BatterySourceModel ()
{
  NS_LOG_FUNCTION (this);
}

void
BatterySourceModel::SetInitialEnergy (double initialEnergyJ)
{
  NS_LOG_FUNCTION (this << initialEnergyJ);
  NS_ASSERT (initialEnergyJ >= 0);
  m_initialEnergyJ = initialEnergyJ;
  m_remainingEnergyJ = m_initialEnergyJ;
}

void
BatterySourceModel::SetNominalCapacity(double nominalCapacity)
{
    NS_LOG_FUNCTION (this<<nominalCapacity);
    NS_ASSERT (nominalCapacity > 0);

    m_nominalCapacity = nominalCapacity;
    m_remainingCapacity = m_nominalCapacity; 
}

void 
BatterySourceModel::SetDrainedCapacity (double drainedCapacity)
{
  NS_LOG_FUNCTION (this << drainedCapacity);

  m_drainCapacity = drainedCapacity;
}

void
BatterySourceModel::SetSupplyVoltage (double supplyVoltageV)
{
  NS_LOG_FUNCTION (this << supplyVoltageV);
  m_supplyVoltageV = supplyVoltageV;
}

void
BatterySourceModel::SetEnergyUpdateInterval (Time interval)
{
  NS_LOG_FUNCTION (this << interval);
  m_energyUpdateInterval = interval;
}

Time
BatterySourceModel::GetEnergyUpdateInterval (void) const
{
  NS_LOG_FUNCTION (this);
  return m_energyUpdateInterval;
}

double
BatterySourceModel::GetSupplyVoltage (void) const
{
  NS_LOG_FUNCTION (this);
  return m_supplyVoltageV;
}

double
BatterySourceModel::GetInitialEnergy (void) const
{
  NS_LOG_FUNCTION (this);
  return m_initialEnergyJ;
}

double
BatterySourceModel::GetNominalCapacity (void) const
{
    NS_LOG_FUNCTION(this);

    return m_nominalCapacity;
}

double
BatterySourceModel::GetRemainingEnergy (void)
{
  NS_LOG_FUNCTION (this);
  // update energy source to get the latest remaining energy.
  UpdateEnergySource ();
  return m_remainingEnergyJ;
}

double
BatterySourceModel::GetRemainingCapacity (void)
{
    NS_LOG_FUNCTION (this);
    // update energy source to get the latest remaining energy.
    UpdateEnergySource ();

    return m_remainingCapacity;
}

double
BatterySourceModel::GetEnergyFraction (void)
{
  NS_LOG_FUNCTION (this);
  // update energy source to get the latest remaining energy.
  UpdateEnergySource ();
  return m_remainingEnergyJ / m_initialEnergyJ;
}

void
BatterySourceModel::UpdateEnergySource (void)
{
  NS_LOG_FUNCTION (this);
  NS_LOG_DEBUG ("BatteryEnergySource:Updating remaining energy.");

  double remainingEnergy = m_remainingEnergyJ;
  CalculateRemainingEnergy ();

  m_lastUpdateTime = Simulator::Now ();

  if (!m_depleted && m_remainingEnergyJ <= m_lowBatteryTh * m_initialEnergyJ)
    {
      m_depleted = true;
      HandleEnergyDrainedEvent ();
    }
  else if (m_depleted && m_remainingEnergyJ > m_highBatteryTh * m_initialEnergyJ)
    {
      m_depleted = false;
      HandleEnergyRechargedEvent ();
    }
  else if (m_remainingEnergyJ != remainingEnergy)
    {
      NotifyEnergyChanged ();
    }

  if (m_energyUpdateEvent.IsExpired ())
    {
      // m_energyUpdateEvent = Simulator::Schedule (m_energyUpdateInterval,
      //                                            &EnergySource::UpdateEnergySource,
      //                                            this);
      NS_LOG_DEBUG("Auto refreshing mechanism is removed.");
    }
}

void
BatterySourceModel::UpdateEnergySource(double drain)
{
  NS_LOG_FUNCTION (this);
  NS_LOG_DEBUG ("BatteryEnergySource:Updating remaining Capacity.");

  double remainingCapacity = m_remainingCapacity;
  m_remainingCapacity = m_remainingCapacity - drain;

  NS_LOG_INFO (std::setprecision(20)<<"Capacity drained: "<<drain<<" Remaining capacity: "<<m_remainingCapacity);
  
  if (!m_depleted && m_remainingCapacity <= 0.0)
    {
      m_depleted = true;
      HandleEnergyDrainedEvent ();
    }
  else if (m_depleted && m_remainingCapacity > m_nominalCapacity)
    {
      m_depleted = false;
      HandleEnergyRechargedEvent ();
    }
  else if (m_remainingCapacity != remainingCapacity)
    {
      NotifyEnergyChanged ();
    }
}

/*
 * Private functions start here.
 */

void
BatterySourceModel::DoInitialize (void)
{
  NS_LOG_FUNCTION (this);
  UpdateEnergySource ();  // start periodic update
}

void
BatterySourceModel::DoDispose (void)
{
  NS_LOG_FUNCTION (this);
  BreakDeviceEnergyModelRefCycle ();  // break reference cycle
}

void
BatterySourceModel::HandleEnergyDrainedEvent (void)
{
  NS_LOG_FUNCTION (this);
  NS_LOG_DEBUG ("BatterySourceModel:Energy depleted!");
  NotifyEnergyDrained (); // notify DeviceEnergyModel objects
}

void
BatterySourceModel::HandleEnergyRechargedEvent (void)
{
  NS_LOG_FUNCTION (this);
  NS_LOG_DEBUG ("BatterySourceModel:Energy recharged!");
  NotifyEnergyRecharged (); // notify DeviceEnergyModel objects
}

void
BatterySourceModel::CalculateRemainingEnergy (void)
{
  NS_LOG_FUNCTION (this);
  double totalCurrentA = CalculateTotalCurrent ();
  Time duration = Simulator::Now () - m_lastUpdateTime;
  NS_ASSERT (duration.IsPositive ());
  // energy = current * voltage * time
  double energyToDecreaseJ = (totalCurrentA * m_supplyVoltageV * duration.GetNanoSeconds ()) / 1e9;
  NS_ASSERT (m_remainingEnergyJ >= energyToDecreaseJ);
  m_remainingEnergyJ -= energyToDecreaseJ;
  NS_LOG_DEBUG ("BatterySourceModel:Remaining energy = " << m_remainingEnergyJ);
}

void
BatterySourceModel::CalculateRemainingCapacity (void)
{
    NS_LOG_FUNCTION (this);
    //must comeback to this
    NS_LOG_DEBUG ("BatterySourceModel:Remaining Capacity = " << m_remainingCapacity);
}

} // namespace ns3