

#include "./lorawan-net-device.h"

namespace ns3{
namespace lorawan{

NS_LOG_COMPONENT_DEFINE("LoRaWANNetDevice");

NS_OBJECT_ENSURE_REGISTERED(LoRaWANNetDevice);

TypeId 
LoRaWANNetDevice::GetTypeId(void){
    static TypeId tid = TypeId("ns3::LoRaWANNetDevice")
                .SetParent<Object> ()
                .SetGroupName("lorawan")
                .AddConstructor<LoRaWANNetDevice> ();
    return tid;
}

LoRaWANNetDevice::LoRaWANNetDevice()
{
}

LoRaWANNetDevice::~LoRaWANNetDevice()
{

}

} // lorawan
} // ns3