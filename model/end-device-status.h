/*
 * Copyright (c) 2018 University of Padova
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Martina Capuzzo <capuzzom@dei.unipd.it>
 *          Davide Magrin <magrinda@dei.unipd.it>
 */

#ifndef END_DEVICE_STATUS_H
#define END_DEVICE_STATUS_H

#include "ns3/object.h"
#include "ns3/lora-net-device.h"
#include "ns3/lora-device-address.h"
#include "ns3/lorawan-mac-header.h"
#include "ns3/class-a-end-device-lorawan-mac.h"
#include "ns3/lora-frame-header.h"
#include "ns3/pointer.h"
#include "ns3/lora-frame-header.h"
#include "ns3/loramac-end-device.h"
#include <iostream>

namespace ns3 {
namespace lorawan {

/**
 * This class represents the Network Server's knowledge about an End Device in
 * the LoRaWAN network it is administering.
 *
 * The Network Server's NetworkStatus component contains a list of instances of
 * this class, one for each device in the network. Each instance contains all
 * the parameters and information of the end device and the packets received
 * from it. Furthermore, this class holds the reply packet that the network
 * server will send to this device at the first available receive window. Upon
 * new packet arrivals at the Network Server, the OnReceivedPacket method is
 * called to update the information regarding the last received packet and its
 * parameters.
 *
 */

/*
 * Diagram of the end-device-status data structure. One instance of this class
 * for each ED, that will be identified by its address.
 *
 * Public Access:
 *
 *  (ED address) --- Current device parameters:
 *                   - First Receive Window SF and DRRe
 *                   - First Receive Window frequency
 *                   - Second Window SF and DR
 *                   - Second Receive Window frequency
 *               --- Reply
 *                   - Need for reply (true/false)
 *                   - Updated reply
 *               --- Received Packets
 *                   - Received packets list (see below).
 *
 *
 * Private Access:
 *
 *  (Received packets list) - List of gateways that received the packet (see below)
 *                          - SF of the received packet
 *                          - Frequency of the received packet
 *                          - Bandwidth of the received packet
 *
 *  (Gateway list) - Time at which the packet was received
 *                 - Reception power
 */

class EndDeviceStatus : public Object
{

public:
  /********************/
  /* Reply management */
  /********************/

  /**
   * Structure representing the reply that the network server will send this
   * device at the first opportunity.
   */
  struct Reply
  {
    // The Mac Header to attach to the reply packet.
    LorawanMacHeader macHeader;

    // The Frame Header to attach to the reply packet.
    LoraFrameHeader frameHeader;

    // The data packet that will be sent as a reply.
    Ptr<Packet> payload;

    // Whether or not this device needs a reply
    bool needsReply = false; //<! On Downlink messages this parameter 
                             //<! is used as check param for retrans messages
  };
  

  enum ClassType {
    CLASS_A = 0,
    CLASS_C = 2
  };

  /**
   * Whether the end device needs a reply.
   *
   * This is determined by looking at headers and payload of the Reply
   * structure: if they are empty, no reply should be needed.
   *
   * \return A boolean value signaling if the end device needs a reply.
   */
  bool NeedsReply (void);

  /**
   * Get the reply packet.
   *
   * \return A pointer to the packet reply (data + headers).
   */
  Ptr<Packet> GetCompleteReplyPacket (void);

  /**
   * @brief Sort out downlink packet 
   * 
   * @return Ptr<Packet> 
   */
  Ptr<Packet> GetCompleteDownlinkPacket (void);

  /**
   * Get the reply packet mac header.
   *
   * \return The packet reply mac header.
   */
  LorawanMacHeader GetReplyMacHeader (void);

  /**
   * Get the reply packet frame header.
   *
   * \return The packet reply frame header.
   */
  LoraFrameHeader GetReplyFrameHeader (void);

  /**
   * Get the data of the reply packet.
   *
   * \return A pointer to the packet reply.
   */
  Ptr<Packet> GetReplyPayload (void);

  /***********************************/
  /* Received packet list management */
  /***********************************/

  /**
   * Structure saving information regarding the packet reception in
   * each gateway.
   */
  struct PacketInfoPerGw
  {
    Address gwAddress;     //!< Address of the gateway that received the packet.
    Time receivedTime;     //!< Time at which the packet was received by this gateway.
    double rxPower;        //!< Reception power of the packet at this gateway.
  };

  // List of gateways, with relative information
  typedef std::map<Address, PacketInfoPerGw> GatewayList;

  /**
   * Structure saving information regarding all packet receptions.
   */
  struct ReceivedPacketInfo
  {
    // Members
    Ptr<Packet const> packet = 0;   //!< The received packet
    GatewayList gwList;      //!< List of gateways that received this packet.
    uint8_t sf;
    double frequency;
    uint8_t nbTrans; //!< Maximum number of retransmision allowed on NS.
  };

  typedef std::list<std::pair<Ptr<Packet const>, ReceivedPacketInfo> >
    ReceivedPacketList;

  typedef std::pair<Ptr<Packet const>, ReceivedPacketInfo> LastReceivedPacketInfo_t;


  /*******************************************/
  /* Proper EndDeviceStatus class definition */
  /*******************************************/

  static TypeId GetTypeId (void);

  EndDeviceStatus ();
  EndDeviceStatus (LoraDeviceAddress endDeviceAddress,
                   Ptr<LoRaMacEndDevice> endDeviceMac);

  EndDeviceStatus (LoraDeviceAddress endDeviceAddress, 
                   Ptr<LoRaMacEndDevice> endDeviceMac, uint8_t devClass);
  
  virtual ~EndDeviceStatus ();

  /**
   * Get the spreading factor this device is using in the first receive window.
   *
   * \return An unsigned 8-bit integer containing the spreading factor.
   */
  uint8_t GetFirstReceiveWindowSpreadingFactor (void);

  /**
   * Get the first window frequency of this device.
   */
  double GetFirstReceiveWindowFrequency (void);

  /**
   * Get the offset of spreading factor this device is using in the second
   * receive window with respect to the first receive window.
   *
   * \return An unsigned 8-bit integer containing the spreading factor.
   */
  uint8_t GetSecondReceiveWindowOffset (void);

  /**
   * Return the second window frequency of this device.
   *
   */
  double GetSecondReceiveWindowFrequency (void);

  /**
   * @brief Get the Second Receive Window Data Rate
   * 
   * @return uint8_t 
   */
  uint8_t GetSecondReceiveWindowDataRate (void);

  /**
   * @brief Get the Second Receive Window Spreading Factor
   * 
   * @return uint8_t 
   */
  uint8_t GetSecondReceiveWindowSpreadingFactor (void);

  /**
   * Get the received packet list.
   *
   * \return The received packet list.
   */
  ReceivedPacketList GetReceivedPacketList (void);

  /**
   * Set the spreading factor this device is using in the first receive window.
   */
  void SetFirstReceiveWindowSpreadingFactor (uint8_t sf);

  /**
   * Set the first window frequency of this device.
   */
  void SetFirstReceiveWindowFrequency (double frequency);

  /**
   * Set the spreading factor this device is using in the first receive window.
   */
  void SetSecondReceiveWindowOffset (uint8_t offset);

  /**
   * Set the second window frequency of this device.
   */
  void SetSecondReceiveWindowFrequency  (double frequency);

  /**
   * Set the reply packet mac header.
   */
  void SetReplyMacHeader (LorawanMacHeader macHeader);

  /**
   * Set the reply packet frame header.
   */
  void SetReplyFrameHeader (LoraFrameHeader frameHeader);

  /**
   * Set the packet reply payload.
   */
  void SetReplyPayload (Ptr<Packet> replyPayload);

  /**
   * @brief Get the Device Class object
   * 
   * @return uint8_t 
   */
  uint8_t GetDeviceClass (void);

  /**
   * @brief On DeviceModeInd command will change or set 
   * device class mode.
   * 
   * @param devClass 
   */
  void ChangeDeviceClass (uint8_t devClass);

  /**
   * @brief Set Class C frame timeout
   * 
   * @param dTime 
   */
  void SetCFrameTimeOut (double dTime);

  /**
   * @brief Get Class C frame timeout
   * 
   * @return double 
   */
  double GetCFrameTimeOut (void);

  /**
   * @brief Set the Max NbTrans 
   * 
   * @param NbTrans 
   */
  void SetMaxNbTrans (uint8_t NbTrans);

  /**
   * @brief Get the Max NbTrans
   * 
   * @return uint8_t 
   */
  uint8_t GetMaxNbTrans (void);

  /**
   * @brief Checks last received packet.  If it's max nbTrans is 
   * exceeded , futher process of packet must be ignored.
   * 
   * @return true nbTrans is violated
   * @return false NS will accept more reTrans
   */
  bool IsNbTransExceeded (void);

  /**
   * @brief Set the Up Frame Counter
   * 
   * @param fCntUp 
   */
  void SetUpFrameCounter (uint16_t fCntUp);

  /**
   * @brief Increases FCntDown by 1.  
   * 
   * @return uint32_t returns new FCntDown
   */
  uint32_t IncreaseDownFrameCounter (void);

  /**
   * @brief Get the Up Frame Counter
   * 
   * @return uint32_t 
   */
  uint32_t GetUpFrameCounter (void);

  /**
   * @brief Get the Down Frame Counter
   * 
   * @return uint32_t 
   */
  uint32_t GetDownFrameCounter (void);

  /**
   * @brief Set the new packet arrival time.  
   * 
   */
  void SetNewPacketArrivalTime (void);

  /**
   * @brief Get the New Packet Arrival Time 
   * 
   * @return Time 
   */
  Time GetNewPacketArrivalTime (void);

  /**
   * @brief Calling this function will ignore effect of IgnorePackets 
   * fucntion
   * 
   */
  void AcceptPackets (void);

  /**
   * @brief Ignore further packets that arrive for this EndDevice
   * 
   */
  void IgnorePackets (void);

  /**
   * @brief Check whether EndDevice accept or ignore packets
   * 
   * @return true EndDevice will accept packets
   * @return false EndDevice will ignore packets
   */
  bool DoesIgnorePackets (void);

  Ptr<LoRaMacEndDevice> GetMac (void);

  //////////////////////
  //  Other methods  //
  //////////////////////

  /**
   * Insert a received packet in the packet list.
   */
  void InsertReceivedPacket (Ptr<Packet const> receivedPacket,
                             const Address& gwAddress);

  /**
   * Return the last packet that was received from this device.
   */
  Ptr<Packet const> GetLastPacketReceivedFromDevice (void);

  /**
   * Return the information about the last packet that was received from the
   * device.
   */
  EndDeviceStatus::ReceivedPacketInfo GetLastReceivedPacketInfo (void);

  /**
   * Initialize reply.
   */
  void InitializeReply (void);

  /**
   * @brief Initialize downlink.
   * 
   */
  void InitializeDownlink (void);

  /**
   * Add MAC command to the list.
   */
  void AddMACCommand (Ptr<MacCommand> macCommand);

  /**
   * @brief checking wether acknowledged is received or not
   * 
   * @return true 
   * @return false 
   */
  bool IsAcked(void);

  /**
   * @brief Set the Ack
   * 
   * @param tf 
   */
  void SetAck (bool tf);

  /**
   * @brief checking whether device need ack or not
   * 
   * @return true 
   * @return false 
   */
  bool NeedAck (void);

  /**
   * @brief Set the Need Ack
   * 
   * @param tf 
   */
  void SetNeedAck (bool tf);

  /**
   * @brief Updates gateway list with new info
   * 
   * @param gwAddress 
   * @param rcvTime 
   * @param rcvPower 
   */
  void UpdateListOfGateways(Address gwAddress, Time rcvTime, double rcvPower);

  /**
   * Return an ordered list of the best gateways.
   */
  std::map<double, Address> GetPowerGatewayMap (void);

  struct Reply m_reply;   //<! Next reply intended for this device

  struct Reply m_downlinkPacket; //<! Packet structure for downlink packets

  EventId m_classCSendEvent;

  EventId m_classCTimeoutEvent;

  LoraDeviceAddress m_endDeviceAddress;   //<! The address of this device

  friend std::ostream& operator<< (std::ostream& os, const EndDeviceStatus& status);

private:
  // Receive window data
  uint8_t m_firstReceiveWindowSpreadingFactor = 0;
  double m_firstReceiveWindowFrequency = 0;
  uint8_t m_secondReceiveWindowOffset = 0;
  double m_secondReceiveWindowFrequency = 869.525;
  uint8_t m_secondReceiveWindowSpreadingFactor = 12;
  uint8_t m_secondReceiveWindowDataRate = 0;


  // List of gateways that ED found a connection with.
  GatewayList m_gatewayList;

  // End Device class type
  ClassType m_devClass;

  ReceivedPacketList m_receivedPacketList;   //<! List of received packets

  // Class C frame timeout 
  double m_cFrameTimeOut;

  // Class C frame timeout event
  EventId m_cFrameTimeOutEvent;

  // Maximum number of retransmission
  uint8_t m_nbTrans;

  // Indication whether an ack is received.
  bool m_isAcked = false;

  // Does EndDevice awaits ack message.
  bool m_needAck = false;

  // Uplink data message counter
  uint32_t m_fCntUp;

  // Downlink data message counter
  uint32_t m_fCntDown;

  // Arrival time of the new packet
  Time m_newPacketArrivalTime;

  // When set it will ignore furthure packets arrived
  bool m_ignorePackets = false;

  // NOTE Using this attribute is 'cheating', since we are assuming perfect
  // synchronization between the info at the device and at the network server
  Ptr<LoRaMacEndDevice> m_mac;   //!< Pointer to the MAC layer of this device
};
}

}
#endif /* DEVICE_STATUS_H */
