

#ifndef MULTICAST_GROUP_H
#define MULTICAST_GROUP_H

#include "ns3/object.h"
#include "ns3/end-device-status.h"

namespace ns3 {
namespace lorawan {

class MulticastGroup: public EndDeviceStatus
{
public:
    static TypeId GetTypeId (void);

    MulticastGroup();
    virtual ~MulticastGroup();
};



} // namespace lorawan
} // namespace ns3

#endif 	/* MULTICAST_GROUP_H */
