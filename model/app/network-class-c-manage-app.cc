

#include "network-class-c-manage-app.h"

namespace ns3 {
namespace lorawan {


NS_LOG_COMPONENT_DEFINE ("NetworkClassCManageApp");

NS_OBJECT_ENSURE_REGISTERED (NetworkClassCManageApp);


TypeId 
NetworkClassCManageApp::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::NetworkClassCManageApp")
                      .SetParent<Object> ()
                      .AddConstructor<NetworkClassCManageApp> ()
                      .SetGroupName ("lorawan")
                      .AddAttribute ("UniformIntervalRv",
                                      "Underlying time interval uniform dist. random variable",
                                      StringValue ("ns3::UniformRandomVariable"),
                                      MakePointerAccessor (&NetworkClassCManageApp::m_rndInterval),
                                      MakePointerChecker<UniformRandomVariable> ())
                      .AddAttribute ("InitDelayRv",
                                     "Initial delay uniform distro random variable",
                                     StringValue ("ns3::UniformRandomVariable"),
                                     MakePointerAccessor (&NetworkClassCManageApp::m_initialRV),
                                     MakePointerChecker<UniformRandomVariable> ())
                      .AddTraceSource ("DownlinkPacketTrace",
                                       "Trace source on downlink with decleration for retransmission",
                                       MakeTraceSourceAccessor (&NetworkClassCManageApp::m_downlinkPacketTrace),
                                       "ns3::Packet::TracedCallback")
                      ;

  return tid;
}

NetworkClassCManageApp::NetworkClassCManageApp ()
{
  NS_LOG_FUNCTION (this);

  m_initialRV = CreateObject<UniformRandomVariable> ();
  m_rndInterval = CreateObject<UniformRandomVariable> ();

  m_initialRV->SetAttribute ("Min", DoubleValue (5.0));
  m_initialRV->SetAttribute ("Max", DoubleValue (200.0));

  m_rndInterval->SetAttribute ("Min", DoubleValue (60));
  m_rndInterval->SetAttribute ("Max", DoubleValue (72000.0));
}

NetworkClassCManageApp::~NetworkClassCManageApp ()
{
    NS_LOG_FUNCTION (this);
}

////////////////////////
// Setter and Getters //
////////////////////////

void 
NetworkClassCManageApp::SetNetworkStatus (Ptr<NetworkStatus> nStatus)
{
    NS_LOG_FUNCTION (this);
    m_status = nStatus;
}

void 
NetworkClassCManageApp::SetInitDelayRvAttribute (std::string name, const AttributeValue& value)
{
  NS_LOG_FUNCTION_NOARGS ();

  m_initialRV->SetAttribute (name, value);
}

void 
NetworkClassCManageApp::SetIntervalTimeRvAttribute (std::string name, const AttributeValue& value)
{
  NS_LOG_FUNCTION_NOARGS ();

  m_rndInterval->SetAttribute (name, value);
}

void
NetworkClassCManageApp::AssignStream (uint64_t stream)
{
  NS_LOG_FUNCTION_NOARGS ();

  m_initialRV->SetStream (stream);
  m_rndInterval->SetStream (stream);
}

////////////
// Others //
////////////

void 
NetworkClassCManageApp::AddClassCDevice (Ptr<EndDeviceStatus> devStatus)
{
  NS_LOG_FUNCTION (this);
  
  LoraDeviceAddress devAddress = devStatus->m_endDeviceAddress;
  //Adding new EndDeviceStatus to map list
  if (m_deviceClist.find (devAddress) == m_deviceClist.end ())
    {
      double tmp = m_initialRV->GetValue ();
      NS_LOG_DEBUG ("Setting up new class C device on network."<<
                    "Next tx in : "<<tmp<<" seconds");

      devStatus->m_classCSendEvent = Simulator::Schedule (Seconds (tmp/*m_initialRV->GetValue ()*/),
                      &NetworkClassCManageApp::SendDownlink,
                      this,
                      devStatus);

      m_deviceClist.insert(std::pair<LoraDeviceAddress, Ptr<EndDeviceStatus>> (devAddress, devStatus));
    }
}

void
NetworkClassCManageApp::SendDownlink (Ptr<EndDeviceStatus> devStatus)
{
    NS_LOG_FUNCTION (this << devStatus->m_endDeviceAddress);

    if (devStatus->m_classCTimeoutEvent.IsRunning () && !devStatus->IsAcked ())
    {
      NS_LOG_DEBUG ("Last packet is still processing. ignore new packets");

      devStatus->m_classCSendEvent = Simulator::Schedule (Seconds(m_rndInterval->GetValue ()),
                                                        &NetworkClassCManageApp::SendDownlink,
                                                        this,
                                                        devStatus);
      return;
    }

    Address gwAddress = m_status->GetBestGatewayForDevice (devStatus->m_endDeviceAddress);

    if (gwAddress == Address ())
    {
      NS_LOG_DEBUG ("No suitable gateway found. Planning for next try.");
      // Scheduling next try
      devStatus->m_classCSendEvent = Simulator::Schedule (Seconds(m_rndInterval->GetValue ()),
                                                        &NetworkClassCManageApp::SendDownlink,
                                                        this,
                                                        devStatus);
      return;
    }
    else
    {
      NS_LOG_DEBUG ("downlink C class confirmed packet will send");


      Ptr<Packet> packet = this->OrganizeDownlinkForDevice (devStatus);
      m_status->SendThroughGateway (packet, gwAddress);

      // firing trace source
      m_downlinkPacketTrace (packet, devStatus->m_downlinkPacket.needsReply);
      // Since every class c downlinks need ack
      devStatus->SetNeedAck (true);

      // Reset the reply
      // devStatus->InitializeDownlink ();
      // when packet ack receives downlink will get reset
    }

    // Setting next transmission
    devStatus->m_classCSendEvent = Simulator::Schedule (Seconds(m_rndInterval->GetValue ()),
                                                        &NetworkClassCManageApp::SendDownlink,
                                                        this,
                                                        devStatus);

    devStatus->m_classCTimeoutEvent = Simulator::Schedule (Seconds(devStatus->GetCFrameTimeOut ()),
                                                           &NetworkClassCManageApp::HandleClassCFrameTimeOutExpireEvent,
                                                           this,
                                                           devStatus);
    
    
}

void
NetworkClassCManageApp::HandleClassCFrameTimeOutExpireEvent (Ptr<EndDeviceStatus> devStatus)
{
  NS_LOG_FUNCTION_NOARGS ();

  if (!devStatus->IsAcked ())
    {
      NS_LOG_DEBUG ("Ack has not been received. reTransmission is in order.");

      devStatus->m_downlinkPacket.needsReply = true; //<! it is an indication for retrans

      this->SendDownlink (devStatus);
    }
}

Ptr<Packet> 
NetworkClassCManageApp::OrganizeDownlinkForDevice (Ptr<EndDeviceStatus> devStatus)
{
  NS_LOG_FUNCTION_NOARGS ();

  // Get the reply packet
  Ptr<Packet> packet = devStatus->GetCompleteDownlinkPacket ();

  // Apply the appropriate tag
  LoraTag tag;
  tag.SetDataRate (devStatus->GetSecondReceiveWindowDataRate ());
  tag.SetFrequency (devStatus->GetSecondReceiveWindowFrequency ());
  tag.SetSpreadingFactor (devStatus->GetSecondReceiveWindowSpreadingFactor ());

  packet->AddPacketTag (tag);
  return packet;
}


}// namespace lorawan
}// namespace ns3
