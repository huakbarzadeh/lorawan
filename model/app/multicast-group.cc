

#include "ns3/multicast-group.h"

namespace ns3 {
namespace lorawan {

NS_OBJECT_ENSURE_REGISTERED (MulticastGroup);

NS_LOG_COMPONENT_DEFINE ("MulticastGroup");

TypeId 
MulticastGroup::GetTypeId (void)
{
  static TypeId tid = TypeId("ns3::MulticastGroup")
                      .SetGroupName ("lorawan")
                      .SetParent<EndDeviceStatus> ()
                      .AddConstructor<MulticastGroup> ();
  return tid;
}

MulticastGroup::MulticastGroup ()
{
  NS_LOG_FUNCTION_NOARGS ();
}

MulticastGroup::~MulticastGroup ()
{
  NS_LOG_FUNCTION_NOARGS ();
}


}// namespace lorawan
}// namespace ns3
