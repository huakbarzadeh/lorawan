

#ifndef CLASS_C_HANDLER_APP_H
#define CLASS_C_HANDLER_APP_H

#include "ns3/object.h"
#include "ns3/net-device.h"
#include "ns3/point-to-point-net-device.h"
#include "ns3/packet.h"
#include "ns3/lora-device-address.h"
#include "ns3/node-container.h"

#include "ns3/log.h"
#include "ns3/loramac-end-device.h"
#include "ns3/end-device-status.h"
#include "ns3/network-status.h"
#include <map>

namespace ns3 {
namespace lorawan {

class NetworkStatus; // Forward decleration

class NetworkClassCManageApp : public Object
{
public:
  static TypeId GetTypeId (void);

  NetworkClassCManageApp ();
  virtual ~NetworkClassCManageApp ();


  ////////////////////////
  // Setter and Getters //
  ////////////////////////

  void SetNetworkStatus (Ptr<NetworkStatus> nStatus);

  void SetInitDelayRvAttribute (std::string, const AttributeValue&);

  void SetIntervalTimeRvAttribute (std::string, const AttributeValue&);

  void AssignStream (uint64_t);

  ////////////
  // Others //
  ////////////

  void AddClassCDevice (Ptr<EndDeviceStatus> devStatus);

  void SendDownlink (Ptr<EndDeviceStatus> devStatus);

  void HandleClassCFrameTimeOutExpireEvent (Ptr<EndDeviceStatus>);

  Ptr<Packet> OrganizeDownlinkForDevice (Ptr<EndDeviceStatus>);

protected:
  Ptr<NetworkStatus> m_status;

  TracedCallback<Ptr<const Packet>, bool> m_downlinkPacketTrace;

  std::map<LoraDeviceAddress, Ptr<EndDeviceStatus>> m_deviceClist;

  Ptr<UniformRandomVariable> m_rndInterval; //<! Random variable steam for time interval 
                                           //   between each event of device

  Ptr<UniformRandomVariable> m_initialRV; //<! Random variable stream for initial delay.
};

} // namespace lorawan
} // namespace ns3



#endif /* CLASS_C_HANDLER_APP_H */