/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2017 University of Padova
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Davide Magrin <magrinda@dei.unipd.it>
 */

#include "ns3/event-driven-app.h"
#include "ns3/pointer.h"
#include "ns3/log.h"
#include "ns3/double.h"
#include "ns3/string.h"
#include "ns3/lora-net-device.h"

namespace ns3 {
namespace lorawan {

NS_LOG_COMPONENT_DEFINE ("EventDrivenApp");

NS_OBJECT_ENSURE_REGISTERED (EventDrivenApp);

TypeId
EventDrivenApp::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::EventDrivenApp")
    .SetParent<Application> ()
    .AddConstructor<EventDrivenApp> ()
    .SetGroupName ("lorawan")
    .AddAttribute ("PacketInterval",
                   "Time delay between each packet transition.",
                   StringValue ("ns3::UniformRandomVariable[Min=60.0|Max=86400"),
                   MakePointerAccessor(&EventDrivenApp::m_intervalDelayRV),
                   MakePointerChecker<RandomVariableStream> ())
                   ;

  return tid;
}

EventDrivenApp::EventDrivenApp ()
  : m_initialDelay (Seconds (1)),
  m_basePktSize (10),
  m_pktSizeRV (0),
  m_initialDelayRV (0)

{
  NS_LOG_FUNCTION_NOARGS ();
}

EventDrivenApp::~EventDrivenApp ()
{
  NS_LOG_FUNCTION_NOARGS ();
}

void
EventDrivenApp::SetInitialDelay (Time delay)
{
  NS_LOG_FUNCTION (this << delay);
  m_initialDelay = delay;
}


void
EventDrivenApp::SetPacketSize (uint8_t size)
{
  m_basePktSize = size;
}


void
EventDrivenApp::SendPacket (void)
{
  NS_LOG_FUNCTION (this);

  // Create and send a new packet
  Ptr<Packet> packet;
  if (m_pktSizeRV)
    {
      int randomsize = m_pktSizeRV->GetInteger ();
      packet = Create<Packet> (m_basePktSize + randomsize);
    }
  else
    {
      packet = Create<Packet> (m_basePktSize);
    }
  m_mac->Send (packet);

  // Schedule the next SendPacket event
  m_sendEvent = Simulator::Schedule (Seconds (m_intervalDelayRV->GetValue ()), 
                                     &EventDrivenApp::SendPacket, this);

  NS_LOG_DEBUG ("Sent a packet of size " << packet->GetSize ());
}

void
EventDrivenApp::StartApplication (void)
{
  NS_LOG_FUNCTION (this);

  // Make sure we have a MAC layer
  if (m_mac == 0)
    {
      // Assumes there's only one device
      Ptr<LoraNetDevice> loraNetDevice = m_node->GetDevice (0)->GetObject<LoraNetDevice> ();

      m_mac = loraNetDevice->GetMac ();
      NS_ASSERT (m_mac != 0);
    }

  // Schedule the next SendPacket event
  Simulator::Cancel (m_sendEvent);

  if (m_initialDelayRV)
  {
    //auto tmp = m_initialDelayRV->GetInteger ();
    NS_LOG_DEBUG ("Starting up application with a first event with a " <<
                  m_initialDelay.GetSeconds ()<< " seconds delay");
    m_sendEvent = Simulator::Schedule (m_initialDelay,
                                      &EventDrivenApp::SendPacket, this);
  }
  else
  {
    NS_LOG_DEBUG ("Starting up application with a first event with a " <<
                  m_initialDelay.GetSeconds ()<< " seconds delay");
    m_sendEvent = Simulator::Schedule (m_initialDelay,
                                      &EventDrivenApp::SendPacket, this);
  }  
  NS_LOG_DEBUG ("Event Id: " << m_sendEvent.GetUid ());
}

void
EventDrivenApp::StopApplication (void)
{
  NS_LOG_FUNCTION_NOARGS ();
  Simulator::Cancel (m_sendEvent);
}

}
}
