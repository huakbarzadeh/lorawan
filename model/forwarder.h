/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2017 University of Padova
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Davide Magrin <magrinda@dei.unipd.it>
 */

#ifndef FORWARDER_H
#define FORWARDER_H

#include "ns3/application.h"
#include "ns3/lora-net-device.h"
#include "ns3/point-to-point-net-device.h"
#include "ns3/nstime.h"
#include "ns3/attribute.h"
#include "ns3/pointer.h"
#include "ns3/jit-queue.h"
#include "ns3/gateway-lorawan-mac.h"

#include <map>

namespace ns3 {
namespace lorawan {

/**
 * This application forwards packets between NetDevices:
 * LoraNetDevice -> PointToPointNetDevice and vice versa.
 */
class Forwarder : public Application
{
public:
  Forwarder ();
  ~Forwarder ();

  static TypeId GetTypeId (void);

  /**
   * Attach a queue to the CsmaNetDevice.
   *
   * The CsmaNetDevice "owns" a queue.  This queue may be set by higher
   * level topology objects to implement a particular queueing method such as
   * DropTail.
   *
   * \see Queue
   * \see DropTailQueue
   * \param queue a Ptr to the queue for being assigned to the device.
   */
  void SetQueue (Ptr<Queue<Packet> > );

  /**
   * Get a copy of the attached Queue.
   *
   * \return a pointer to the queue.
   */
  Ptr<Queue<Packet> > GetQueue (void) const;

  /**
   * Sets the device to use to communicate with the EDs.
   *
   * \param loraNetDevice The LoraNetDevice on this node.
   */
  void SetLoraNetDevice (Ptr<LoraNetDevice> loraNetDevice);

  /**
   * Sets the P2P device to use to communicate with the NS.
   *
   * \param pointToPointNetDevice The P2PNetDevice on this node.
   */
  void SetPointToPointNetDevice (Ptr<PointToPointNetDevice> pointToPointNetDevice);

  /**
   * @brief Set the Phy Tx Ready State 
   * 
   * @param txReady if true forwarder can send to gateway
   */
  void SetPhyTxReadyState (bool txReady);

  /**
   * @brief Set the Jit Q Checking Interval
   * 
   */
  void SetJitQCheckingInterval (Time);

  /**
   * @brief Get the Jit Q Checking Interval 
   * 
   * @return Time 
   */
  Time GetJitQCheckingInterval (void) const;

  /**
   * Receive a packet from the LoraNetDevice.
   *
   * \param loraNetDevice The LoraNetDevice we received the packet from.
   * \param packet The packet we received.
   * \param protocol The protocol number associated to this packet.
   * \param sender The address of the sender.
   * \returns True if we can handle the packet, false otherwise.
   */
  bool ReceiveFromLora (Ptr<NetDevice> loraNetDevice, Ptr<const Packet> packet,
                        uint16_t protocol, const Address& sender);

  /**
   * Receive a packet from the PointToPointNetDevice
   */
  bool ReceiveFromPointToPoint (Ptr<NetDevice> pointToPointNetDevice,
                                Ptr<const Packet> packet, uint16_t protocol,
                                const Address& sender);

  /**
   * Start the application
   */
  void StartApplication (void);

  /**
   * Stop the application
   */
  void StopApplication (void);

private:
  void JitQueueDownlinkUpdate (void);

  Ptr<LoraNetDevice> m_loraNetDevice; //!< Pointer to the node's LoraNetDevice

  Ptr<GatewayLorawanMac> m_mac; //!< Pointer to the node's Mac layer

  /**
   * The Queue which this CsmaNetDevice uses as a packet source.
   * Management of this Queue has been delegated to the CsmaNetDevice
   * and it has the responsibility for deletion.
   * \see class Queue
   * \see class DropTailQueue
   */
  Ptr<Queue<Packet> > m_queue;

  Ptr<PointToPointNetDevice> m_pointToPointNetDevice; //!< Pointer to the
                                                      //!P2PNetDevice we use to
                                                      //!communicate with the NS

  bool m_isPhyTxReady;

  Time m_jitQueueIntervalCheck;

  Time m_dutyCycleWaitTimeEvent;

  EventId m_forwarderCheckingEvent;

  EventId m_txScheduleEvent; //!< Event for handling tx on scheduled time

  EventId m_dutyCycleDelayedEvent; //!< Event sets when a duty cycle waiting time 
                                   //!< must be in order

  TracedCallback<Ptr<const Packet> > m_forwarderTxDropTrace;
};

} //namespace ns3

}
#endif /* FORWARDER */
