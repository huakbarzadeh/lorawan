
// temporary, must get rid of after build
#include "ns3/loramac-end-device.h"

namespace ns3{
namespace lorawan{


NS_LOG_COMPONENT_DEFINE("LoRaMacEndDevice");
NS_OBJECT_ENSURE_REGISTERED(LoRaMacCtx);
NS_OBJECT_ENSURE_REGISTERED (LoRaMacEndDevice);

//LoRaMacCtx
TypeId 
LoRaMacCtx::GetTypeId(void)
{
    static TypeId tid = TypeId("ns3::LoRaMacCtx")
                        .SetParent<Object> ()
                        .SetGroupName ("lorawan")
                        .AddConstructor<LoRaMacCtx> ()
                        .AddAttribute ("MacState",
                                        "Internal Mac layer in integer.",
                                        UintegerValue(0),
                                        MakeUintegerAccessor(&LoRaMacCtx::m_macState),
                                        MakeUintegerChecker<uint32_t> ())
                        .AddAttribute ("AppPort",
                                        "Application port number.",
                                        UintegerValue(0), //must change
                                        MakeUintegerAccessor(&LoRaMacCtx::m_port),
                                        MakeUintegerChecker<uint8_t> ())
                        ;
    return tid;
}

LoRaMacCtx::LoRaMacCtx()
{
    NS_LOG_FUNCTION(this);
}

LoRaMacCtx::~LoRaMacCtx()
{
    
}

// LoRaMacEndDevice
TypeId 
LoRaMacEndDevice::GetTypeId(void)
{
    static TypeId tid = TypeId("ns3::LoRaMacEndDevice")
        .SetParent<Object> ()
        .SetGroupName("lorawan")
        .AddConstructor<LoRaMacEndDevice> ();
    
    return tid;
}

LoRaMacEndDevice::LoRaMacEndDevice()
{
    NS_LOG_FUNCTION(this);
    //Lorawan Mac layer initialization
    NS_LOG_INFO("LoraMac Initialized.");
}

LoRaMacEndDevice::~LoRaMacEndDevice()
{
    NS_LOG_FUNCTION(this);
}

} // namespace ns3

} // namespace lorawan