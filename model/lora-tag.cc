/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2017 University of Padova
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Davide Magrin <magrinda@dei.unipd.it>
 */

#include "ns3/lora-tag.h"
#include "ns3/uinteger.h"

namespace ns3
{
  namespace lorawan
  {

    NS_OBJECT_ENSURE_REGISTERED(LoraTag);

    TypeId
    LoraTag::GetTypeId(void)
    {
      static TypeId tid = TypeId("ns3::LoraTag")
                              .SetParent<Tag>()
                              .SetGroupName("lorawan")
                              .AddConstructor<LoraTag>();
      return tid;
    }

    TypeId
    LoraTag::GetInstanceTypeId(void) const
    {
      return GetTypeId();
    }

    LoraTag::LoraTag(uint8_t sf, uint8_t destroyedBy) : m_sf(sf),
                                                        m_destroyedBy(destroyedBy),
                                                        m_receivePower(0),
                                                        m_dataRate(0),
                                                        m_frequency(0),
                                                        m_txSchedule (Seconds (0))
    {
    }

    LoraTag::~LoraTag()
    {
    }

    uint32_t
    LoraTag::GetSerializedSize(void) const
    {
      // Each datum about a SF is 1 byte + receivePower (the size of a double) +
      // frequency (the size of a double) + shcedule time (the size of a double)
      return 3 + 3 * sizeof(double);
    }

    void
    LoraTag::Serialize(TagBuffer i) const
    {
      i.WriteU8(m_sf);
      i.WriteU8(m_destroyedBy);
      i.WriteDouble(m_receivePower);
      i.WriteU8(m_dataRate);
      i.WriteDouble(m_frequency);
      i.WriteDouble (m_txSchedule.GetSeconds ());
    }

    void
    LoraTag::Deserialize(TagBuffer i)
    {
      m_sf = i.ReadU8();
      m_destroyedBy = i.ReadU8();
      m_receivePower = i.ReadDouble();
      m_dataRate = i.ReadU8();
      m_frequency = i.ReadDouble();
      m_txSchedule = Seconds(i.ReadDouble ());
    }

    void
    LoraTag::Print(std::ostream &os) const
    {
      os <<"*** LoRaTag ***\nSF: "<< m_sf << " DestroySF: " << m_destroyedBy << 
            " ReceivePower: " << m_receivePower << "\nDataRate: " << m_dataRate
            <<" scheduled tx for :"<<m_txSchedule <<"\n";
    }

    uint8_t
    LoraTag::GetSpreadingFactor() const
    {
      return m_sf;
    }

    uint8_t
    LoraTag::GetDestroyedBy() const
    {
      return m_destroyedBy;
    }

    double
    LoraTag::GetReceivePower() const
    {
      return m_receivePower;
    }

    void
    LoraTag::SetDestroyedBy(uint8_t sf)
    {
      m_destroyedBy = sf;
    }

    void
    LoraTag::SetSpreadingFactor(uint8_t sf)
    {
      m_sf = sf;
    }

    void
    LoraTag::SetReceivePower(double receivePower)
    {
      m_receivePower = receivePower;
    }

    void
    LoraTag::SetFrequency(double frequency)
    {
      m_frequency = frequency;
    }

    double
    LoraTag::GetFrequency(void)
    {
      return m_frequency;
    }

    uint8_t
    LoraTag::GetDataRate(void)
    {
      return m_dataRate;
    }

    void
    LoraTag::SetDataRate(uint8_t dataRate)
    {
      m_dataRate = dataRate;
    }

    void 
    LoraTag::SetTxDownlinkSchedule (Time txTime)
    {
      this->m_txSchedule = txTime;
    }

    Time
    LoraTag::GetTxDownlinkSchedule (void)
    {
      return this->m_txSchedule;
    }

    ////////////////
    //LoRaClassTag//
    ////////////////
    NS_OBJECT_ENSURE_REGISTERED(LoRaClassTag);

    TypeId
    LoRaClassTag::GetTypeId(void)
    {
      static TypeId tid = TypeId("ns3::LoRaClassTag")
                              .SetParent<Tag>()
                              .SetGroupName("lorawan")
                              .AddConstructor<LoRaClassTag>();
      return tid;
    }

    TypeId
    LoRaClassTag::GetInstanceTypeId(void) const
    {
      return GetTypeId();
    }

    LoRaClassTag::LoRaClassTag()
    {
    }

    LoRaClassTag::LoRaClassTag(uint8_t dmode) : m_deviceMode(dmode)
    {
    }

    LoRaClassTag::~LoRaClassTag()
    {
    }

    uint32_t
    LoRaClassTag::GetSerializedSize(void) const
    {
      // Each datum about a SF is 1 byte + receivePower (the size of a double) +
      // frequency (the size of a double)
      return 1;
    }

    void
    LoRaClassTag::Serialize(TagBuffer i) const
    {
      i.WriteU8(m_deviceMode);
    }

    void
    LoRaClassTag::Deserialize(TagBuffer i)
    {
      m_deviceMode = i.ReadU8();
    }

    void
    LoRaClassTag::Print(std::ostream &os) const
    {
      m_deviceMode == 0x0 ? os << "Class A device" : os << "Class C device";
    }

    void
    LoRaClassTag::SetDeviceMode(uint8_t dmode)
    {
      m_deviceMode = dmode;
    }

    uint8_t
    LoRaClassTag::GetDeviceMode(void)
    {
      return m_deviceMode;
    }


    /////////////////////////////////
    //Confirmed Down Acknoledgement//
    /////////////////////////////////
    NS_OBJECT_ENSURE_REGISTERED(ConfDownAckTag);

    TypeId
    ConfDownAckTag::GetTypeId(void)
    {
      static TypeId tid = TypeId("ns3::ConfDownAckTag")
                              .SetParent<Tag>()
                              .SetGroupName("lorawan")
                              .AddConstructor<ConfDownAckTag>();
      return tid;
    }

    TypeId
    ConfDownAckTag::GetInstanceTypeId(void) const
    {
      return GetTypeId();
    }

    ConfDownAckTag::ConfDownAckTag()
    {
    }

    ConfDownAckTag::~ConfDownAckTag()
    {
    }

    uint32_t
    ConfDownAckTag::GetSerializedSize(void) const
    {
      // Each datum about a SF is 1 byte + receivePower (the size of a double) +
      // frequency (the size of a double)
      return 1;
    }

    void
    ConfDownAckTag::Serialize(TagBuffer i) const
    {
      i.WriteU8( (uint8_t)true );
    }

    void
    ConfDownAckTag::Deserialize(TagBuffer i)
    {
      i.ReadU8();
    }

    void
    ConfDownAckTag::Print(std::ostream &os) const
    {
      os << "ConfirmedDownLink message ack.";
    }
  } // namespace lorawan
} // namespace ns3
