

#ifndef LORAWAN_NET_DEVICE_H
#define LORAWAN_NET_DEVICE_H

#include "ns3/object.h"
#include "ns3/log.h"

namespace ns3{
namespace lorawan{

class LoRaWANNetDevice : public Object 
{
public:
    static TypeId GetTypeId(void);

    LoRaWANNetDevice();
    virtual ~LoRaWANNetDevice();
private:
};

}
}

#endif /* LORAWAN_NET_DEVICE_H */