/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2017 University of Padova
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Davide Magrin <magrinda@dei.unipd.it>
 */

#include "ns3/forwarder.h"
#include "ns3/log.h"

namespace ns3 {
namespace lorawan {

NS_LOG_COMPONENT_DEFINE ("Forwarder");

NS_OBJECT_ENSURE_REGISTERED (Forwarder);

TypeId
Forwarder::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::Forwarder")
    .SetParent<Application> ()
    .AddConstructor<Forwarder> ()
    .SetGroupName ("lorawan")
    .AddAttribute ("DownLinkQueue",
                   "this is used as a down link queue for forwarder.",
                   PointerValue (),
                   MakePointerAccessor (&Forwarder::m_queue),
                   MakePointerChecker<Queue<Packet> > ())
    .AddAttribute ("QueueCheckInterval",
                   "Time interval, forwarder checks jit queue for downlink.",
                   TimeValue (Seconds (1.0)),
                   MakeTimeAccessor (&Forwarder::SetJitQCheckingInterval,
                                     &Forwarder::GetJitQCheckingInterval),
                   MakeTimeChecker ())
    .AddTraceSource ("ForwarderTxDrop", 
                     "Trace source indicating a packet has been "
                     "dropped by the device during transmission",
                     MakeTraceSourceAccessor (&Forwarder::m_forwarderTxDropTrace),
                     "ns3::Packet::TracedCallback");
  return tid;
}

Forwarder::Forwarder () : 
            m_isPhyTxReady (true),
            m_jitQueueIntervalCheck (Seconds (1.0))
{
  NS_LOG_FUNCTION_NOARGS ();
}

Forwarder::~Forwarder ()
{
  NS_LOG_FUNCTION_NOARGS ();
  m_queue = 0;
}

////////////////////////
// Setter and Getters //
////////////////////////
void
Forwarder::SetPointToPointNetDevice (Ptr<PointToPointNetDevice>
                                     pointToPointNetDevice)
{
  NS_LOG_FUNCTION (this << pointToPointNetDevice);

  m_pointToPointNetDevice = pointToPointNetDevice;
}

void
Forwarder::SetLoraNetDevice (Ptr<LoraNetDevice> loraNetDevice)
{
  NS_LOG_FUNCTION (this << loraNetDevice);

  m_loraNetDevice = loraNetDevice;
  m_mac = m_loraNetDevice->GetMac ()->GetObject<GatewayLorawanMac> ();
}

void
Forwarder::SetPhyTxReadyState (bool txReady)
{
  m_isPhyTxReady = txReady;

  if (m_isPhyTxReady)
  {
    if (m_queue->IsEmpty ())
      {
        NS_LOG_DEBUG ("No packet for sending!!!!");
        return;
      }
    
    Ptr<const Packet> pkt = m_queue->GetObject<JitQueue> ()->JitPeek ();
    LoraTag pktTag;
    NS_ASSERT_MSG (pkt->PeekPacketTag (pktTag), "Packet is not Lora Packet");

    NS_LOG_DEBUG ("Downlink at frequency: "<<pktTag.GetFrequency ());

    if ((pktTag.GetTxDownlinkSchedule () - Simulator::Now ()).GetSeconds ()
                                          < m_jitQueueIntervalCheck.GetSeconds ())
      {
        NS_LOG_DEBUG ("It too early for packet to be sent");
        return;
      }

    if (m_mac->GetWaitingTime (pktTag.GetFrequency ()) == 0)
      {
        NS_LOG_DEBUG ("Sending downlink packet.");

        m_loraNetDevice->Send (m_queue->Dequeue ());
      }
    
    if (m_queue->IsEmpty ())
      {
        NS_LOG_DEBUG ("Queue has got empty. further queue check will"<<
                            " get ignore untill downlink.");
        m_txScheduleEvent.Cancel ();
      }
  }
}

void 
Forwarder::SetJitQCheckingInterval (Time interval)
{
  NS_LOG_FUNCTION (this);

  m_jitQueueIntervalCheck = interval;
}


Time
Forwarder::GetJitQCheckingInterval (void) const
{
  NS_LOG_FUNCTION (this);

  return m_jitQueueIntervalCheck;
}

void
Forwarder::SetQueue (Ptr<Queue<Packet> > q)
{
  NS_LOG_FUNCTION (q);
  m_queue = q;
}

Ptr<Queue<Packet> >
Forwarder::GetQueue (void) const 
{ 
  NS_LOG_FUNCTION_NOARGS ();
  return m_queue;
}

////////////
// Others //
////////////
bool
Forwarder::ReceiveFromLora (Ptr<NetDevice> loraNetDevice, Ptr<const Packet>
                            packet, uint16_t protocol, const Address& sender)
{
  NS_LOG_FUNCTION (this << packet << protocol << sender);

  NS_LOG_DEBUG ("Forwarder: Packet from LoRa toward NS.");

  Ptr<Packet> packetCopy = packet->Copy ();

  m_pointToPointNetDevice->Send (packetCopy,
                                 m_pointToPointNetDevice->GetBroadcast (),
                                 0x800);

  return true;
}

bool
Forwarder::ReceiveFromPointToPoint (Ptr<NetDevice> pointToPointNetDevice,
                                    Ptr<const Packet> packet, uint16_t protocol,
                                    const Address& sender)
{
  NS_LOG_FUNCTION (this << packet << protocol << sender);

  NS_LOG_DEBUG ("Packet from NS toward LoRa Gateway.");

  Ptr<Packet> packetCopy = packet->Copy ();

  if (m_queue->Enqueue (packetCopy))
    {
      // New packet successfully enqueued.
      if (m_mac->IsTransmitting ())
        {
          NS_LOG_DEBUG ("Gateway currently is transmitting.");
          // After finishing transmission Forwarder::SetPhyTxReadyState 
          // will handle dequeue and rest of process.
        }
      else
        {
          Ptr<const Packet> pkt = m_queue->GetObject<JitQueue> ()->JitPeek ();
          LoraTag pktTag;
          NS_ASSERT_MSG (pkt->PeekPacketTag (pktTag), "Packet is not Lora Packet");

          NS_LOG_DEBUG ("Downlink at frequency: "<<pktTag.GetFrequency ());

          if ((pktTag.GetTxDownlinkSchedule () - Simulator::Now ()).GetSeconds ()
                                         <= m_jitQueueIntervalCheck.GetSeconds ())
            {
              // If duty cycle restriction allows send a downlink
              if (m_mac->GetWaitingTime (pktTag.GetFrequency ()) == 0)
                {
                  NS_LOG_DEBUG ("Sending downlink");
                  m_loraNetDevice->Send (m_queue->Dequeue ());
                }
            }
        }
      
      if (m_queue->GetCurrentSize ().GetValue () == (uint32_t)1)
        {
          NS_LOG_DEBUG ("Queue is loaded.  Forwarder will frequenty check for"<<
                          " downlink possiblity.");
          m_txScheduleEvent = Simulator::Schedule (m_jitQueueIntervalCheck,
                                          &Forwarder::JitQueueDownlinkUpdate,
                                          this);
        }
      if (m_queue->IsEmpty ())
        {
          NS_LOG_DEBUG ("Queue has got empty. further queue check will"<<
                              " get ignore untill downlink.");
          m_txScheduleEvent.Cancel ();
        }
    }
  else 
    {
      NS_LOG_DEBUG ("Packet has been droped before enqueue. "<<
                    "Proper callbacks must be handled in queue.");
      return false;
    }

  return true; // because it really does not matter
}

void
Forwarder::StartApplication (void)
{
  NS_LOG_FUNCTION (this);

  // TODO Make sure we are connected to both needed devices
}

void
Forwarder::StopApplication (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  // TODO Get rid of callbacks
}

void
Forwarder::JitQueueDownlinkUpdate (void)
{
  NS_LOG_FUNCTION (this);

  if (m_mac->IsTransmitting ())
    {
      NS_LOG_DEBUG ("Gateway currently is transmitting.");
      // After finishing transmission Forwarder::SetPhyTxReadyState 
      // will handle dequeue and rest of process.
    }
  else
    {
      Ptr<const Packet> pkt = m_queue->GetObject<JitQueue> ()->JitPeek ();
      LoraTag pktTag;
      NS_ASSERT_MSG (pkt->PeekPacketTag (pktTag), "Packet is not Lora Packet");

      NS_LOG_DEBUG ("Downlink at frequency: "<<pktTag.GetFrequency ());

      if ((pktTag.GetTxDownlinkSchedule () - Simulator::Now ()).GetSeconds ()
                                      <= m_jitQueueIntervalCheck.GetSeconds ())
        {
          // If duty cycle restriction allows send a downlink
          if (m_mac->GetWaitingTime (pktTag.GetFrequency ()) == 0)
            {
              NS_LOG_DEBUG ("Sending downlink");
              m_loraNetDevice->Send (m_queue->Dequeue ());
            }
        }
    }
  
  if (!m_queue->IsEmpty ())
    {
      NS_LOG_DEBUG ("Setting up next update event");
      m_txScheduleEvent = Simulator::Schedule (m_jitQueueIntervalCheck,
                                      &Forwarder::JitQueueDownlinkUpdate,
                                      this);
    }
  else
    {
      NS_LOG_DEBUG ("Queue has got empty. further queue check will"<<
                          " get ignore untill new downlink.");
      m_txScheduleEvent.Cancel ();
    }
}

}
}
