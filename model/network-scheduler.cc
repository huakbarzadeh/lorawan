#include "network-scheduler.h"

namespace ns3 {
namespace lorawan {

NS_LOG_COMPONENT_DEFINE ("NetworkScheduler");

NS_OBJECT_ENSURE_REGISTERED (NetworkScheduler);

TypeId
NetworkScheduler::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::NetworkScheduler")
    .SetParent<Object> ()
    .AddConstructor<NetworkScheduler> ()
    .AddAttribute ("PacketBuffDelay",
                   "between the first and last duplicate"
                   "for buffer",
                   TimeValue (MilliSeconds (200)),
                   MakeTimeAccessor (&NetworkScheduler::m_packetBufferDelay),
                   MakeTimeChecker ())
    .AddTraceSource ("ReceiveWindowOpened",
                     "Trace source that is fired when a receive window opportunity happens.",
                     MakeTraceSourceAccessor (&NetworkScheduler::m_receiveWindowOpened),
                     "ns3::Packet::TracedCallback")
    .SetGroupName ("lorawan");
  return tid;
}

NetworkScheduler::NetworkScheduler () : 
    m_packetBufferDelay (MilliSeconds (200))
{
}

NetworkScheduler::NetworkScheduler (Ptr<NetworkStatus> status,
                                    Ptr<NetworkController> controller) :
  m_status (status),
  m_controller (controller),
  m_packetBufferDelay (MilliSeconds (200))
{
}

NetworkScheduler::~NetworkScheduler ()
{
}

void
NetworkScheduler::OnReceivedPacket (Ptr<const Packet> packet)
{
  NS_LOG_FUNCTION (packet);

  // Get the current packet's frame counter
  Ptr<Packet> packetCopy = packet->Copy ();
  LorawanMacHeader receivedMacHdr;
  packetCopy->RemoveHeader (receivedMacHdr);
  LoraFrameHeader receivedFrameHdr;
  receivedFrameHdr.SetAsUplink ();
  packetCopy->RemoveHeader (receivedFrameHdr);
  uint16_t currentFrameCounter = receivedFrameHdr.GetFCnt ();
  

  // Getting the EndDeviceStatus
  Ptr<EndDeviceStatus> edStatus = m_status->GetEndDeviceStatus (packet);

  NS_ASSERT_MSG (edStatus, "EndDevice does not exist in network list.");

  if ( (edStatus->GetUpFrameCounter () & 0xffff) < currentFrameCounter)
    {
      NS_LOG_DEBUG ("NetworkScheduler::OnReceivedPacket: New Packet received at NS.");
      // Set timer for new packet.  with this timer server
      // can deduplicate multigateway packets
      edStatus->SetNewPacketArrivalTime ();

      edStatus->AcceptPackets ();

      // - Extract the address
      LoraDeviceAddress deviceAddress = receivedFrameHdr.GetAddress ();

      if (receivedMacHdr.GetMType () != LorawanMacHeader::UNCONFIRMED_DATA_UP)
        {
          // Schedule OnReceiveWindowOpportunity event
          Simulator::Schedule (Seconds (1),
                              &NetworkScheduler::OnReceiveWindowOpportunity,
                              this,
                              deviceAddress,
                              1);     // This will be the first receive window
        }
      
      NS_LOG_DEBUG ("Packet response is scheduled");
    }
  else if ( (edStatus->GetUpFrameCounter () & 0xffff) == currentFrameCounter)
    {
      if ( (Simulator::Now () - edStatus->GetNewPacketArrivalTime ()) > m_packetBufferDelay )
        {
          NS_LOG_DEBUG ("NetworkScheduler::OnReceivedPacket: reTransmission of packet is detected");

          // if not exceeded the limit shall accept retransmission
          // otherwise must ignore futher process
          if (edStatus->IsNbTransExceeded ())  //must get modified
            {
              NS_LOG_DEBUG ("NetworkScheduler::OnReceivedPacket: reTransmission received."<< 
                            "but it will get ignored due overflow of NbTrans");
              edStatus->IgnorePackets ();
            }
          else
            {
              NS_LOG_DEBUG ("NetworkScheduler::OnReceivedPacket: reTransmission received at NS.");
              // Set timer for new packet.  with this timer server
              // can deduplicate multigateway packets
              edStatus->SetNewPacketArrivalTime ();
              edStatus->AcceptPackets ();

              // - Extract the address
              LoraDeviceAddress deviceAddress = receivedFrameHdr.GetAddress ();

              if (receivedMacHdr.GetMType () != LorawanMacHeader::UNCONFIRMED_DATA_UP)
                {
                  // Schedule OnReceiveWindowOpportunity event
                  Simulator::Schedule (Seconds (1),
                                      &NetworkScheduler::OnReceiveWindowOpportunity,
                                      this,
                                      deviceAddress,
                                      1);     // This will be the first receive window
                }
              
              NS_LOG_DEBUG ("Packet response is scheduled for retransmission");
            }
        }
    }
  else 
    {
      NS_LOG_DEBUG ("Further packets will get ignored till packet with right FCnt arrives.");
      edStatus->IgnorePackets ();
    }
}

void
NetworkScheduler::OnReceiveWindowOpportunity (LoraDeviceAddress deviceAddress, int window)
{
  NS_LOG_FUNCTION (deviceAddress);

  NS_LOG_DEBUG ("Opening receive window nubmer " << window << " for device "
                                                 << deviceAddress);

  // Check whether we can send a reply to the device, again by using
  // NetworkStatus
  //Address gwAddress = m_status->GetBestGatewayForDevice (deviceAddress, window);
  Address gwAddress = m_status->GetBestGatewayForDevice (deviceAddress);

  NS_ASSERT_MSG (gwAddress != Address (), "No Gateway found for response");

  NS_LOG_DEBUG ("Found available gateway with address: " << gwAddress);

  //A gateway was found
  m_controller->BeforeSendingReply (m_status->GetEndDeviceStatus
                                      (deviceAddress));

  // Check whether this device needs a response by querying m_status
  bool needsReply = m_status->NeedsReply (deviceAddress);

  if (needsReply)
    {
      NS_LOG_DEBUG ("A reply is needed");

      // Send the reply through that gateway
      m_status->SendThroughGateway (m_status->GetReplyForDevice
                                      (deviceAddress, window),
                                    gwAddress);

      // scheduling second response window
      if (window == 1)
        {
          Simulator::Schedule (Seconds (1),
                              &NetworkScheduler::OnReceiveWindowOpportunity,
                              this,
                              deviceAddress,
                              2);
        }
    }
  
  if (window == 2)
    {
      // Reset the reply
      m_status->GetEndDeviceStatus (deviceAddress)->InitializeReply ();
    }
}

}
}
