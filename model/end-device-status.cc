/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2017 University of Padova
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Martina Capuzzo <capuzzom@dei.unipd.it>
 *          Davide Magrin <magrinda@dei.unipd.it>
 */

#include "ns3/end-device-status.h"
#include "ns3/simulator.h"
#include "ns3/lorawan-mac-header.h"
#include "ns3/lora-frame-header.h"
#include "ns3/log.h"
#include "ns3/pointer.h"
#include "ns3/command-line.h"
#include "ns3/simulator.h"
#include "ns3/packet.h"
#include "ns3/lora-tag.h"

#include <algorithm>

namespace ns3 {
namespace lorawan {

NS_LOG_COMPONENT_DEFINE ("EndDeviceStatus");

TypeId
EndDeviceStatus::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::EndDeviceStatus")
                          .SetParent<Object> ()
                          .AddConstructor<EndDeviceStatus> ()
                          .SetGroupName ("lorawan")
                          .AddAttribute ("CFrameTimeOut",
                                        "Class C frame timeout for resend",
                                        DoubleValue (10.0),
                                        MakeDoubleAccessor(&EndDeviceStatus::m_cFrameTimeOut),
                                        MakeDoubleChecker<double> ())
                          .AddAttribute ("SecondRxWindowSF",
                                         "Spreading Factor for second receive window",
                                         UintegerValue (12),
                                         MakeUintegerAccessor (&EndDeviceStatus::m_firstReceiveWindowSpreadingFactor),
                                         MakeUintegerChecker<uint8_t> ())
                          .AddAttribute ("SecondRxWindowDR",
                                         "Second receive window data rate",
                                         UintegerValue (0),
                                         MakeUintegerAccessor (&EndDeviceStatus::m_secondReceiveWindowDataRate),
                                         MakeUintegerChecker<uint8_t> ())
                      ;
  return tid;
}

EndDeviceStatus::EndDeviceStatus (LoraDeviceAddress endDeviceAddress,
                                  Ptr<LoRaMacEndDevice> endDeviceMac)
    : m_reply (EndDeviceStatus::Reply ()),
      m_endDeviceAddress (endDeviceAddress),
      m_receivedPacketList (ReceivedPacketList ()),
      m_nbTrans (1),
      m_fCntUp (0),
      m_fCntDown (0),
      m_mac (endDeviceMac)
{
  NS_LOG_FUNCTION (endDeviceAddress);

  // default device class is A
  m_devClass = CLASS_A;
}

EndDeviceStatus::EndDeviceStatus (LoraDeviceAddress endDeviceAddress, 
                   Ptr<LoRaMacEndDevice> endDeviceMac, uint8_t devClass)
    : m_reply (EndDeviceStatus::Reply ()),
      m_endDeviceAddress (endDeviceAddress),
      m_receivedPacketList (ReceivedPacketList ()),
      m_nbTrans (1),
      m_fCntUp (0),
      m_fCntDown (0),
      m_mac (endDeviceMac)
{
  NS_LOG_FUNCTION (endDeviceAddress);

  // Setting the device class type
  m_devClass = (ClassType) devClass;
}

EndDeviceStatus::EndDeviceStatus ()
{
  NS_LOG_FUNCTION_NOARGS ();

  // Initialize data structure
  m_reply = EndDeviceStatus::Reply ();
  m_receivedPacketList = ReceivedPacketList ();
}

EndDeviceStatus::~EndDeviceStatus ()
{
  NS_LOG_FUNCTION_NOARGS ();
}

///////////////
//  Getters  //
///////////////

uint8_t
EndDeviceStatus::GetFirstReceiveWindowSpreadingFactor ()
{
  NS_LOG_FUNCTION_NOARGS ();

  return m_firstReceiveWindowSpreadingFactor;
}

double
EndDeviceStatus::GetFirstReceiveWindowFrequency ()
{
  NS_LOG_FUNCTION_NOARGS ();

  return m_firstReceiveWindowFrequency;
}

uint8_t
EndDeviceStatus::GetSecondReceiveWindowOffset ()
{
  NS_LOG_FUNCTION_NOARGS ();

  return m_secondReceiveWindowOffset;
}

uint8_t
EndDeviceStatus::GetDeviceClass (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  return (uint8_t)m_devClass;
}

void 
EndDeviceStatus::ChangeDeviceClass (uint8_t devClass)
{
  NS_LOG_FUNCTION (this << " Change device class to : "<<devClass);

  m_devClass = (ClassType)devClass;
}

double
EndDeviceStatus::GetSecondReceiveWindowFrequency ()
{
  NS_LOG_FUNCTION_NOARGS ();
  return m_secondReceiveWindowFrequency;
}

uint8_t
EndDeviceStatus::GetSecondReceiveWindowDataRate (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  return m_secondReceiveWindowDataRate;
}

uint8_t
EndDeviceStatus::GetSecondReceiveWindowSpreadingFactor (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  return m_secondReceiveWindowSpreadingFactor;
}

Ptr<Packet>
EndDeviceStatus::GetCompleteReplyPacket (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  // Start from reply payload
  Ptr<Packet> replyPacket;
  if (m_reply.payload) // If it has APP data to send
    {
      NS_LOG_DEBUG ("Crafting reply packet from existing payload");
      replyPacket = m_reply.payload->Copy ();
    }
  else // If no APP data needs to be sent, use an empty payload
    {
      NS_LOG_DEBUG ("Crafting reply packet using an empty payload");
      replyPacket = Create<Packet> (0);
    }

  // Add headers
  m_reply.frameHeader.SetAddress (m_endDeviceAddress);
  Ptr<Packet> lastPacket = GetLastPacketReceivedFromDevice ()->Copy ();
  LorawanMacHeader mHdr;
  LoraFrameHeader fHdr;
  fHdr.SetAsUplink ();
  lastPacket->RemoveHeader (mHdr);
  lastPacket->RemoveHeader (fHdr);
  m_reply.frameHeader.SetFCnt (fHdr.GetFCnt ());
  // m_reply.macHeader.SetMType (LorawanMacHeader::UNCONFIRMED_DATA_DOWN);
  replyPacket->AddHeader (m_reply.frameHeader);
  replyPacket->AddHeader (m_reply.macHeader);

  NS_LOG_DEBUG ("Added MAC header" << m_reply.macHeader);
  NS_LOG_DEBUG ("Added frame header" << m_reply.frameHeader);

  return replyPacket;
}

Ptr<Packet>
EndDeviceStatus::GetCompleteDownlinkPacket (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  // Start from reply payload
  Ptr<Packet> downlinkPacket;
  if (m_downlinkPacket.payload) // If it has APP data to send
    {
      NS_LOG_DEBUG ("Crafting downlink packet from existing payload");
      downlinkPacket = m_downlinkPacket.payload->Copy ();
    }
  else // If no APP data needs to be sent, use an empty payload
    {
      NS_LOG_DEBUG ("Crafting reply packet using an empty payload");
      downlinkPacket = Create<Packet> (20);
    }

  // Add headers
  m_downlinkPacket.frameHeader.SetAddress (this->m_endDeviceAddress);
  m_downlinkPacket.frameHeader.SetFCnt ((uint16_t)this->IncreaseDownFrameCounter ());
  m_downlinkPacket.frameHeader.SetAsDownlink ();
  m_downlinkPacket.frameHeader.SetAck (false);
  m_downlinkPacket.macHeader.SetMType (LorawanMacHeader::CONFIRMED_DATA_DOWN);
  downlinkPacket->AddHeader (m_downlinkPacket.frameHeader);
  downlinkPacket->AddHeader (m_downlinkPacket.macHeader);

  NS_LOG_DEBUG ("Added MAC header" << m_downlinkPacket.macHeader);
  NS_LOG_DEBUG ("Added frame header" << m_downlinkPacket.frameHeader);

  return downlinkPacket;
}

bool
EndDeviceStatus::NeedsReply (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  return m_reply.needsReply;
}

LorawanMacHeader
EndDeviceStatus::GetReplyMacHeader ()
{
  NS_LOG_FUNCTION_NOARGS ();
  return m_reply.macHeader;
}

LoraFrameHeader
EndDeviceStatus::GetReplyFrameHeader ()
{
  NS_LOG_FUNCTION_NOARGS ();
  return m_reply.frameHeader;
}

Ptr<Packet>
EndDeviceStatus::GetReplyPayload (void)
{
  NS_LOG_FUNCTION_NOARGS ();
  return m_reply.payload->Copy ();
}

Ptr<LoRaMacEndDevice>
EndDeviceStatus::GetMac (void)
{
  return m_mac;
}

EndDeviceStatus::ReceivedPacketList
EndDeviceStatus::GetReceivedPacketList ()
{
  NS_LOG_FUNCTION_NOARGS ();
  return m_receivedPacketList;
}

void
EndDeviceStatus::SetFirstReceiveWindowSpreadingFactor (uint8_t sf)
{
  NS_LOG_FUNCTION_NOARGS ();
  m_firstReceiveWindowSpreadingFactor = sf;
}

void
EndDeviceStatus::SetFirstReceiveWindowFrequency (double frequency)
{
  NS_LOG_FUNCTION_NOARGS ();
  m_firstReceiveWindowFrequency = frequency;
}

void
EndDeviceStatus::SetSecondReceiveWindowOffset (uint8_t offset)
{
  NS_LOG_FUNCTION_NOARGS ();
  m_secondReceiveWindowOffset = offset;
}

void
EndDeviceStatus::SetSecondReceiveWindowFrequency (double frequency)
{
  NS_LOG_FUNCTION_NOARGS ();
  m_secondReceiveWindowFrequency = frequency;
}

void
EndDeviceStatus::SetReplyMacHeader (LorawanMacHeader macHeader)
{
  NS_LOG_FUNCTION_NOARGS ();
  m_reply.macHeader = macHeader;
}

void
EndDeviceStatus::SetReplyFrameHeader (LoraFrameHeader frameHeader)
{
  NS_LOG_FUNCTION_NOARGS ();
  m_reply.frameHeader = frameHeader;
}

void
EndDeviceStatus::SetReplyPayload (Ptr<Packet> replyPayload)
{
  NS_LOG_FUNCTION_NOARGS ();
  m_reply.payload = replyPayload;
}

bool 
EndDeviceStatus::IsAcked (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  return this->m_isAcked;
}

bool 
EndDeviceStatus::NeedAck (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  return this->m_needAck;
}

void
EndDeviceStatus::SetAck (bool tf)
{
  NS_LOG_FUNCTION_NOARGS ();

  this->m_isAcked = tf;
}

void
EndDeviceStatus::SetNeedAck(bool tf)
{
  NS_LOG_FUNCTION_NOARGS ();

  this->m_needAck = tf;
}

void 
EndDeviceStatus::UpdateListOfGateways(Address gwAddress, Time rcvTime, double rcvPower)
{
  NS_LOG_FUNCTION_NOARGS ();

  auto it = m_gatewayList.find(gwAddress);
  if (it != m_gatewayList.end())
    {
      NS_LOG_DEBUG ("Updating Gateway with new packet information " << it->first);

      it->second.receivedTime = rcvTime;
      it->second.rxPower = rcvPower;

      return;
    }
  
  NS_LOG_DEBUG ("New Gateway received packet" ); // postponed till address get found

  PacketInfoPerGw gwInfo;
  gwInfo.receivedTime = rcvTime;
  gwInfo.rxPower = rcvPower;
  gwInfo.gwAddress = gwAddress;
  m_gatewayList.insert (std::pair<Address, PacketInfoPerGw>(gwAddress, gwInfo) );
}

void 
EndDeviceStatus::SetCFrameTimeOut (double dTime)
{
  NS_LOG_FUNCTION (this);

  m_cFrameTimeOut = dTime;
}

double 
EndDeviceStatus::GetCFrameTimeOut (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  return m_cFrameTimeOut;
}

void 
EndDeviceStatus::SetMaxNbTrans (uint8_t NbTrans)
{
  NS_LOG_FUNCTION (this);

  m_nbTrans = NbTrans;
}

uint8_t
EndDeviceStatus::GetMaxNbTrans (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  return m_nbTrans;
}

bool
EndDeviceStatus::IsNbTransExceeded (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  if (!m_receivedPacketList.empty ())
    {
      if ( m_receivedPacketList.back ().second.nbTrans > 0)
        {
          return false;
        }
      else 
        {
          return true;
        } 
    }
  else 
    {
      NS_ABORT_MSG ("No packet has received for this device");
    }
}

void 
EndDeviceStatus::SetUpFrameCounter (uint16_t fCntUp)
{
  NS_LOG_FUNCTION (this << fCntUp);

  // must carefully add up FCntUp
  // if ( (m_fCntUp & 0xFFFF) < fCntUp)
  //   {

  //   }
  // else
  //   {
  //     /* code */
  //   }
  

  m_fCntUp = (uint32_t)fCntUp;
}

uint32_t
EndDeviceStatus::IncreaseDownFrameCounter (void)
{
  NS_LOG_FUNCTION (this);

  return ++m_fCntDown;
}

uint32_t
EndDeviceStatus::GetUpFrameCounter (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  return m_fCntUp;
}

uint32_t
EndDeviceStatus::GetDownFrameCounter (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  return m_fCntDown;
}

void
EndDeviceStatus::SetNewPacketArrivalTime (void)
{
  NS_LOG_FUNCTION (this);

  m_newPacketArrivalTime = Simulator::Now ();
}

Time 
EndDeviceStatus::GetNewPacketArrivalTime (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  return m_newPacketArrivalTime;
}

void 
EndDeviceStatus::AcceptPackets (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  m_ignorePackets = false;
}

void 
EndDeviceStatus::IgnorePackets (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  m_ignorePackets = true;
}

bool 
EndDeviceStatus::DoesIgnorePackets (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  return m_ignorePackets;
}

///////////////////////
//   Other methods   //
///////////////////////

void
EndDeviceStatus::InsertReceivedPacket (Ptr<Packet const> receivedPacket, const Address &gwAddress)
{
  NS_LOG_FUNCTION_NOARGS ();

  // Create a copy of the packet
  Ptr<Packet> myPacket = receivedPacket->Copy ();

  // Extract the headers
  LorawanMacHeader macHdr;
  myPacket->RemoveHeader (macHdr);

  LoraFrameHeader frameHdr;
  frameHdr.SetAsUplink ();
  myPacket->RemoveHeader (frameHdr);

  // Update current parameters
  LoraTag tag;
  myPacket->RemovePacketTag (tag);
  SetFirstReceiveWindowSpreadingFactor (tag.GetSpreadingFactor ());
  SetFirstReceiveWindowFrequency (tag.GetFrequency ());

  // Update Information on the received packet
  ReceivedPacketInfo info;
  info.sf = tag.GetSpreadingFactor ();
  info.frequency = tag.GetFrequency ();
  info.packet = receivedPacket;

  double rcvPower = tag.GetReceivePower ();

  // since we only keep last received packet on NS only m_lastReceivedPacketInfo
  // will change based on received packet
  if (frameHdr.GetFCnt () == (m_fCntUp & 0xffff))
    {
      NS_LOG_DEBUG ("Received packet is retransmission.");

      NS_ASSERT_MSG (!(m_receivedPacketList.empty ()), "Received packet list is empty");

      GatewayList &gwList = m_receivedPacketList.back ().second.gwList;

      PacketInfoPerGw gwInfo;
      gwInfo.receivedTime = Simulator::Now ();
      gwInfo.rxPower = rcvPower;
      gwInfo.gwAddress = gwAddress;
      gwList.insert (std::pair<Address, PacketInfoPerGw> (gwAddress, gwInfo));
      info.nbTrans--;

      this->UpdateListOfGateways (gwAddress, gwInfo.receivedTime, gwInfo.rxPower);

      NS_LOG_DEBUG ("Size of gateway list: " << gwList.size ());
    }
  else // only frameHdr.GetFCnt () > this->FCntUp is viable
    {
      NS_LOG_DEBUG ("Packet arrived for the first time.");
      // update FCntUp
      m_fCntUp  = frameHdr.GetFCnt () | (m_fCntUp & 0xFFFF0000);
      NS_LOG_DEBUG ("New FCntUp on NS: "<<m_fCntUp);

      m_receivedPacketList.clear ();

      info.nbTrans = m_nbTrans;
      PacketInfoPerGw gwInfo;
      gwInfo.receivedTime = Simulator::Now ();
      gwInfo.rxPower = rcvPower;
      gwInfo.gwAddress = gwAddress;
      info.gwList.insert (std::pair<Address, PacketInfoPerGw> (gwAddress, gwInfo));
      m_receivedPacketList.push_back(std::pair<Ptr<Packet const>, ReceivedPacketInfo> (receivedPacket, info));

      this->UpdateListOfGateways (gwAddress, gwInfo.receivedTime, gwInfo.rxPower);
    }
  
  NS_LOG_DEBUG ("Changes to last packet status organized");
  
  // Perform insertion in list, also checking that the packet isn't already in
  // the list (it could have been received by another GW already)

  // Start searching from the end
  /* auto it = m_receivedPacketList.rbegin ();
  for (; it != m_receivedPacketList.rend (); it++)
    {
      // Get the frame counter of the current packet to compare it with the
      // newly received one
      Ptr<Packet> packetCopy = ((*it).first)->Copy ();
      LorawanMacHeader currentMacHdr;
      packetCopy->RemoveHeader (currentMacHdr);
      LoraFrameHeader currentFrameHdr;
      frameHdr.SetAsUplink ();
      packetCopy->RemoveHeader (currentFrameHdr);

      NS_LOG_DEBUG ("Received packet's frame counter: " << unsigned(frameHdr.GetFCnt ())
                                                        << "\nCurrent packet's frame counter: "
                                                        << unsigned(currentFrameHdr.GetFCnt ()));

      if (frameHdr.GetFCnt () == currentFrameHdr.GetFCnt ())
        {
          NS_LOG_INFO ("Packet was already received by another gateway");

          // This packet had already been received from another gateway:
          // add this gateway's reception information.
          GatewayList &gwList = it->second.gwList;

          PacketInfoPerGw gwInfo;
          gwInfo.receivedTime = Simulator::Now ();
          gwInfo.rxPower = rcvPower;
          gwInfo.gwAddress = gwAddress;
          gwList.insert (std::pair<Address, PacketInfoPerGw> (gwAddress, gwInfo));

          NS_LOG_DEBUG ("Size of gateway list: " << gwList.size ());

          break; // Exit from the cycle
        }
    } 
  if (it == m_receivedPacketList.rend ())
    {
      NS_LOG_INFO ("Packet was received for the first time");
      PacketInfoPerGw gwInfo;
      gwInfo.receivedTime = Simulator::Now ();
      gwInfo.rxPower = rcvPower;
      gwInfo.gwAddress = gwAddress;
      info.gwList.insert (std::pair<Address, PacketInfoPerGw> (gwAddress, gwInfo));
      m_receivedPacketList.push_back (
          std::pair<Ptr<Packet const>, ReceivedPacketInfo> (receivedPacket, info));
    }*/
  // NS_LOG_DEBUG (*this);
}

EndDeviceStatus::ReceivedPacketInfo
EndDeviceStatus::GetLastReceivedPacketInfo (void)
{
  NS_LOG_FUNCTION_NOARGS ();
  auto it = m_receivedPacketList.rbegin ();
  if (it != m_receivedPacketList.rend ())
    {
      return it->second;
    }
  else
    {
      return EndDeviceStatus::ReceivedPacketInfo ();
    }
}

Ptr<Packet const>
EndDeviceStatus::GetLastPacketReceivedFromDevice (void)
{
  NS_LOG_FUNCTION_NOARGS ();
  auto it = m_receivedPacketList.rbegin ();
  if (it != m_receivedPacketList.rend ())
    {
      return it->first;
    }
  else
    {
      return 0;
    }
}

void
EndDeviceStatus::InitializeReply ()
{
  NS_LOG_FUNCTION_NOARGS ();
  m_reply = Reply ();
  m_reply.needsReply = false;
}

void
EndDeviceStatus::InitializeDownlink (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  m_downlinkPacket = Reply ();
  m_downlinkPacket.needsReply = false; //<! Means retransmission is not launched
}

void
EndDeviceStatus::AddMACCommand (Ptr<MacCommand> macCommand)
{
  m_reply.frameHeader.AddCommand (macCommand);
}

std::map<double, Address>
EndDeviceStatus::GetPowerGatewayMap (void)
{
  std::map<double, Address> gatewayPowers;

  if (m_receivedPacketList.empty ())
    {
      NS_LOG_DEBUG ("No gateway has received a packet from ED.");
      return gatewayPowers;
    }

  // Create a map of the gateways
  // Key: received power
  // Value: address of the corresponding gateway
  ReceivedPacketInfo info = m_receivedPacketList.back ().second;
  GatewayList gwList = info.gwList;

  for (auto it = gwList.begin (); it != gwList.end (); it++)
    {
      Address currentGwAddress = (*it).first;
      double currentRxPower = (*it).second.rxPower;
      gatewayPowers.insert (std::pair<double, Address> (currentRxPower, currentGwAddress));
    }

  return gatewayPowers;
}

std::ostream &
operator<< (std::ostream &os, const EndDeviceStatus &status)
{
  os << "Total packets received: " << status.m_receivedPacketList.size () << std::endl;

  for (auto j = status.m_receivedPacketList.begin (); j != status.m_receivedPacketList.end (); j++)
    {
      EndDeviceStatus::ReceivedPacketInfo info = (*j).second;
      EndDeviceStatus::GatewayList gatewayList = info.gwList;
      Ptr<Packet const> pkt = (*j).first;
      os << pkt << " " << gatewayList.size () << std::endl;
      for (EndDeviceStatus::GatewayList::iterator k = gatewayList.begin (); k != gatewayList.end ();
           k++)
        {
          EndDeviceStatus::PacketInfoPerGw infoPerGw = (*k).second;
          os << "  " << infoPerGw.gwAddress << " " << infoPerGw.rxPower << std::endl;
        }
    }

  return os;
}


} // namespace lorawan
} // namespace ns3
