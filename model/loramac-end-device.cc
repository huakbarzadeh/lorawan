
#include "ns3/loramac-end-device.h"

namespace ns3
{
  namespace lorawan
  {

    NS_OBJECT_ENSURE_REGISTERED(LoRaMacEndDevice);

    NS_LOG_COMPONENT_DEFINE("LoRaMacEndDevice");

    TypeId
    LoRaMacEndDevice::GetTypeId(void)
    {
      static TypeId tid = TypeId("ns3::LoRaMacEndDevice")
                              .SetParent<EndDeviceLorawanMac>()
                              .SetGroupName("lorawan")
                              .AddConstructor<LoRaMacEndDevice>()
                              .AddAttribute("ClassType",
                                            "Device Class Type.",
                                            EnumValue(CLASS_A),
                                            MakeEnumAccessor(&LoRaMacEndDevice::SetDeviceClassType),
                                            MakeEnumChecker(CLASS_A, "Class A",
                                                            CLASS_C, "Class C"));
      return tid;
    }

    LoRaMacEndDevice::LoRaMacEndDevice() : m_devClassConf(false),
                                           // LoRaWAN default
                                           m_receiveDelay1(Seconds(1)),
                                           // LoRaWAN default
                                           m_receiveDelay2(Seconds(2)),
                                           m_rx1DrOffset(0)
    {
      NS_LOG_FUNCTION(this);
    }

    LoRaMacEndDevice::~LoRaMacEndDevice()
    {
      NS_LOG_FUNCTION(this);
    }

    ///////////////////////
    //Setters and Getters//
    ///////////////////////
    void
    LoRaMacEndDevice::SetDeviceClassType(enum class_t type)
    {
      NS_LOG_FUNCTION(this);

      m_devClass = type;
    }

    uint8_t
    LoRaMacEndDevice::GetDeviceClassType()
    {
      return (uint8_t)m_devClass;
    }

    void
    LoRaMacEndDevice::SendToPhy(Ptr<Packet> packetToSend)
    {
      /////////////////////////////////////////////////////////
      // Add headers, prepare TX parameters and send the packet
      /////////////////////////////////////////////////////////

      // NS_LOG_DEBUG("PacketToSend: " << packetToSend);

      // Big question mark

      // Data Rate Adaptation as in LoRaWAN specification, V1.0.2 (2016)
      if (m_enableDRAdapt && (m_dataRate > 0) && (m_retxParams.retxLeft < m_maxNumbTx) && (m_retxParams.retxLeft % 2 == 0))
      {
        m_txPower = 14; // Reset transmission power
        m_dataRate = m_dataRate - 1;
      }

      //NS_ASSERT_MSG(m_devClass != 0, "Device class not specified.");

      // Craft LoraTxParameters object
      LoraTxParameters params;
      params.sf = GetSfFromDataRate(m_dataRate);
      params.headerDisabled = m_headerDisabled;
      params.codingRate = m_codingRate;
      params.bandwidthHz = GetBandwidthFromDataRate(m_dataRate);
      params.nPreamble = m_nPreambleSymbols;
      params.crcEnabled = 1;
      params.lowDataRateOptimizationEnabled = 0;

      // Wake up PHY layer and directly send the packet

      Ptr<LogicalLoraChannel> txChannel = GetChannelForTx();

      m_txChannel = txChannel;

      NS_LOG_DEBUG("PacketToSend: " << packetToSend);
      m_phy->Send(packetToSend, params, txChannel->GetFrequency(), m_txPower);

      //////////////////////////////////////////////
      // Register packet transmission for duty cycle
      //////////////////////////////////////////////

      // Compute packet duration
      Time duration = m_phy->GetOnAirTime(packetToSend, params);

      // Register the sent packet into the DutyCycleHelper
      m_channelHelper.AddEvent(duration, txChannel);

      //////////////////////////////
      // Prepare for the downlink //
      //////////////////////////////

      if (this->m_devClass == CLASS_C)
      {
        // Switch the PHY to the channel so that it will listen here for downlink
        m_phy->GetObject<EndDeviceLoraPhy>()->SetFrequency(GetSecondReceiveWindowFrequency());

        // Instruct the PHY on the right Spreading Factor to listen for during the window
        // create a SetReplyDataRate function?
        uint8_t replyDataRate = GetSecondReceiveWindowDataRate();
        NS_LOG_DEBUG("m_dataRate: " << unsigned(m_dataRate) << ", m_rx1DrOffset: " << unsigned(m_rx1DrOffset) << ", replyDataRate: " << unsigned(replyDataRate) << ".");

        m_phy->GetObject<EndDeviceLoraPhy>()->SetSpreadingFactor(GetSfFromDataRate(replyDataRate));
      }
    }

    //////////////////////////
    //  Receiving methods   //
    //////////////////////////
    void
    LoRaMacEndDevice::Receive(Ptr<Packet const> packet)
    {
      NS_LOG_FUNCTION(this << packet);

      // Work on a copy of the packet
      Ptr<Packet> packetCopy = packet->Copy();

      // Remove the Mac Header to get some information
      LorawanMacHeader mHdr;
      packetCopy->RemoveHeader(mHdr); 

      NS_LOG_DEBUG("Mac Header: " << mHdr);

      // Only keep analyzing the packet if it's downlink
      if (!mHdr.IsUplink())
      {
        NS_LOG_INFO("Found a downlink packet.");

        // Remove the Frame Header
        LoraFrameHeader fHdr;
        fHdr.SetAsDownlink();
        packetCopy->RemoveHeader(fHdr);

        NS_LOG_DEBUG("Frame Header: " << fHdr);

        // Determine whether this packet is for us
        bool messageForUs = ((m_address == fHdr.GetAddress ()) || (m_multicastAddress ==
                                           fHdr.GetAddress () && m_devClass == CLASS_C));

        if (messageForUs)
        {
          NS_LOG_INFO("The message is for us!");

          uint8_t mType = mHdr.GetMType();

          resetRetransmissionParameters ();

          ChangePhyStateEnd ();

          switch (mType)
          {
            case (uint8_t)LorawanMacHeader::CONFIRMED_DATA_DOWN:
              NS_LOG_INFO("CONFIRMED_DATA_DOWN received further process will persue.");
              ConfirmedDownAck ();
              break;
            case (uint8_t)LorawanMacHeader::UNCONFIRMED_DATA_DOWN:
              NS_LOG_INFO("UNCONFIRMED_DATA_DOWN received further process is ignored.");
              break;
            default:
              break;
          }
          // If it exists, cancel the second receive window event
          // THIS WILL BE GetReceiveWindow()
          Simulator::Cancel(m_secondReceiveWindow);

          // Parse the MAC commands
          ParseCommands(fHdr);

          // TODO Pass the packet up to the NetDevice
          // Call the trace source
          m_receivedPacket(packet);
        }
        else
        {
          NS_LOG_DEBUG("The message is intended for another recipient.");

          // In this case, we are either receiving in the first receive window
          // and finishing reception inside the second one, or receiving a
          // packet in the second receive window and finding out, after the
          // fact, that the packet is not for us. In either case, if we no
          // longer have any retransmissions left, we declare failure.
          if (m_devClass == CLASS_A && m_secondReceiveWindow.IsExpired()) // DownLink Class A not for us
          {  
            m_phy->GetObject<EndDeviceLoraPhy> ()->SwitchToSleep (); 
            if (m_retxParams.waitingAck)
            {
              if (m_retxParams.retxLeft == 0)
              {
                uint8_t txs = m_maxNumbTx - (m_retxParams.retxLeft);
                m_requiredTxCallback(txs, false, m_retxParams.firstAttempt, m_retxParams.packet);
                NS_LOG_DEBUG("Failure: no more retransmissions left. Used " << unsigned(txs) << " transmissions.");

                // Reset retransmission parameters
                resetRetransmissionParameters();
              }
              else // Reschedule
              {
                this->Send(m_retxParams.packet);
                NS_LOG_INFO("We have " << unsigned(m_retxParams.retxLeft) << " retransmissions left: rescheduling transmission.");
              }
            }
          }
          else if (m_devClass == CLASS_C && m_closeFirstWindow.IsExpired ())
          {
            ChangePhyStateEnd ();
            if (m_retxParams.waitingAck)
            {
              if (m_retxParams.retxLeft == 0)
              {
                uint8_t txs = m_maxNumbTx - (m_retxParams.retxLeft);
                m_requiredTxCallback(txs, false, m_retxParams.firstAttempt, m_retxParams.packet);
                NS_LOG_DEBUG("Failure: no more retransmissions left. Used " << unsigned(txs) << " transmissions.");

                // Reset retransmission parameters
                resetRetransmissionParameters();
              }
              else // Reschedule
              {
                this->Send(m_retxParams.packet);
                NS_LOG_INFO("We have " << unsigned(m_retxParams.retxLeft) << " retransmissions left: rescheduling transmission.");
              }
            }
          }
        }
      }
      else if (m_secondReceiveWindow.IsExpired() && m_devClass == CLASS_A)
      {
        NS_LOG_INFO("The packet we are receiving is in uplink.");
        m_phy->GetObject<EndDeviceLoraPhy> ()->SwitchToSleep ();
        if (m_retxParams.waitingAck)
        {
          if (m_retxParams.retxLeft > 0)
          {
            this->Send(m_retxParams.packet);
            NS_LOG_INFO("We have " << unsigned(m_retxParams.retxLeft) << " retransmissions left: rescheduling transmission.");
          }
          else
          {
            uint8_t txs = m_maxNumbTx - (m_retxParams.retxLeft);
            m_requiredTxCallback(txs, false, m_retxParams.firstAttempt, m_retxParams.packet);
            NS_LOG_DEBUG("Failure: no more retransmissions left. Used " << unsigned(txs) << " transmissions.");

            // Reset retransmission parameters
            resetRetransmissionParameters();
          }
        }
      }
      else if (m_closeFirstWindow.IsExpired () && m_devClass == CLASS_C)
      {
        NS_LOG_INFO("The packet we are receiving is in uplink.");
        
        ChangePhyStateEnd ();

        if (m_retxParams.waitingAck)
        {
          if (m_retxParams.retxLeft > 0)
          {
            this->Send(m_retxParams.packet);
            NS_LOG_INFO("We have " << unsigned(m_retxParams.retxLeft) << " retransmissions left: rescheduling transmission.");
          }
          else
          {
            uint8_t txs = m_maxNumbTx - (m_retxParams.retxLeft);
            m_requiredTxCallback(txs, false, m_retxParams.firstAttempt, m_retxParams.packet);
            NS_LOG_DEBUG("Failure: no more retransmissions left. Used " << unsigned(txs) << " transmissions.");

            // Reset retransmission parameters
            resetRetransmissionParameters();
          }
        }
      }
    }

    void
    LoRaMacEndDevice::FailedReception(Ptr<Packet const> packet)
    {
      NS_LOG_FUNCTION(this << packet);

      if (m_devClass == CLASS_C && m_closeFirstWindow.IsExpired ())
      {
        // Set PHY in Standby mode
        ChangePhyStateEnd ();

        if (m_retxParams.waitingAck)
        {
          if (m_retxParams.retxLeft > 0)
          {
            this->Send(m_retxParams.packet);
            NS_LOG_INFO("We have " << unsigned(m_retxParams.retxLeft) << " retransmissions left: rescheduling transmission.");
          }
          else
          {
            uint8_t txs = m_maxNumbTx - (m_retxParams.retxLeft);
            m_requiredTxCallback(txs, false, m_retxParams.firstAttempt, m_retxParams.packet);
            NS_LOG_DEBUG("Failure: no more retransmissions left. Used " << unsigned(txs) << " transmissions.");

            // Reset retransmission parameters
            resetRetransmissionParameters();
          }
        }
      }
      else if (m_devClass == CLASS_A && m_secondReceiveWindow.IsExpired())
      {
        m_phy->GetObject<EndDeviceLoraPhy> ()->SwitchToSleep ();
        if (m_retxParams.waitingAck)
        {
          if (m_retxParams.retxLeft > 0)
          {
            this->Send(m_retxParams.packet);
            NS_LOG_INFO("We have " << unsigned(m_retxParams.retxLeft) << " retransmissions left: rescheduling transmission.");
          }
          else
          {
            uint8_t txs = m_maxNumbTx - (m_retxParams.retxLeft);
            m_requiredTxCallback(txs, false, m_retxParams.firstAttempt, m_retxParams.packet);
            NS_LOG_DEBUG("Failure: no more retransmissions left. Used " << unsigned(txs) << " transmissions.");

            // Reset retransmission parameters
            resetRetransmissionParameters();
          }
        }
      }
    }

    void
    LoRaMacEndDevice::TxFinished(Ptr<const Packet> packet)
    {
      NS_LOG_FUNCTION_NOARGS();

      // Schedule the opening of the first receive window
      Simulator::Schedule(m_receiveDelay1,
                          &LoRaMacEndDevice::OpenFirstReceiveWindow, this);

      if (m_devClass == CLASS_A)
      {
        // Switch the PHY to sleep
        // Schedule the opening of the second receive window
        m_secondReceiveWindow = Simulator::Schedule(m_receiveDelay2,
                                                    &LoRaMacEndDevice::OpenSecondReceiveWindow,
                                                    this);

      }
    }

    void
    LoRaMacEndDevice::OpenFirstReceiveWindow(void)
    {
      NS_LOG_FUNCTION_NOARGS();

      NS_LOG_DEBUG ("Open First Receive Window.");

      // Switch the PHY to the channel so that it will listen here for downlink
      m_phy->GetObject<EndDeviceLoraPhy>()->SetFrequency(m_txChannel->GetFrequency());

      // Instruct the PHY on the right Spreading Factor to listen for during the window
      // create a SetReplyDataRate function?
      uint8_t replyDataRate = GetFirstReceiveWindowDataRate();
      NS_LOG_DEBUG("m_dataRate: " << unsigned(m_dataRate) << ", m_rx1DrOffset: " << unsigned(m_rx1DrOffset) << ", replyDataRate: " << unsigned(replyDataRate) << ".");

      m_phy->GetObject<EndDeviceLoraPhy>()->SetSpreadingFactor(GetSfFromDataRate(replyDataRate));

      // Set Phy in Standby mode
      m_phy->GetObject<EndDeviceLoraPhy>()->SwitchToRx();

      //Calculate the duration of a single symbol for the first receive window DR
      double tSym = pow(2, GetSfFromDataRate(GetFirstReceiveWindowDataRate())) * 2 / GetBandwidthFromDataRate(GetFirstReceiveWindowDataRate());

      // Schedule return to sleep after "at least the time required by the end
      // device's radio transceiver to effectively detect a downlink preamble"
      // (LoraWAN specification)
      m_closeFirstWindow = Simulator::Schedule(Seconds(m_receiveWindowDurationInSymbols * tSym),
                                               &LoRaMacEndDevice::CloseFirstReceiveWindow, this); //m_receiveWindowDuration
    }

    void
    LoRaMacEndDevice::CloseFirstReceiveWindow(void)
    {
      NS_LOG_FUNCTION_NOARGS();
      NS_LOG_DEBUG ("Close First Receive Window.");

      Ptr<EndDeviceLoraPhy> phy = m_phy->GetObject<EndDeviceLoraPhy>();

      // Check the Phy layer's state:
      // - RX -> We are receiving a preamble.
      // - STANDBY -> Nothing was received.
      // - SLEEP -> We have received a packet.
      // We should never be in TX or SLEEP mode at this point
      switch (phy->GetState())
      {
      case EndDeviceLoraPhy::TX:
        NS_ABORT_MSG("PHY was in TX mode when attempting to "
                     << "close a receive window.");
        break;
      case EndDeviceLoraPhy::RX:
        // if PHY is receiving: let it finish. The Receive method will switch it back to SLEEP.
        // otherwise proceed like STANDBY
        if (!phy->IsReceiving ())
        {
          // Make proper changes to PHY state
          ChangePhyStateEnd ();
          if (m_devClass == CLASS_C) // Class C devices retransmission is started
          {
            if (m_retxParams.waitingAck)
            {
              NS_LOG_DEBUG("No reception initiated by PHY: rescheduling transmission.");
              if (m_retxParams.retxLeft > 0)
              {
                NS_LOG_INFO("We have " << unsigned(m_retxParams.retxLeft) << " retransmissions left: rescheduling transmission.");
                this->Send(m_retxParams.packet);
              }

              else if (m_retxParams.retxLeft == 0)// && m_phy->GetObject<EndDeviceLoraPhy>()->GetState() != EndDeviceLoraPhy::RX)
              {
                uint8_t txs = m_maxNumbTx - (m_retxParams.retxLeft);
                m_requiredTxCallback(txs, false, m_retxParams.firstAttempt, m_retxParams.packet);
                NS_LOG_DEBUG("Failure: no more retransmissions left. Used " << unsigned(txs) << " transmissions.");

                // Reset retransmission parameters
                resetRetransmissionParameters();
              }

              else
              {
                NS_ABORT_MSG("The number of retransmissions left is negative ! ");
              }
            }
            else
            {
              uint8_t txs = m_maxNumbTx - (m_retxParams.retxLeft);
              m_requiredTxCallback(txs, true, m_retxParams.firstAttempt, m_retxParams.packet);
              NS_LOG_INFO("We have " << unsigned(m_retxParams.retxLeft) << " transmissions left. We were not transmitting confirmed messages.");

              // Reset retransmission parameters
              resetRetransmissionParameters();
            }
          }
        }
        break;
      case EndDeviceLoraPhy::SLEEP:
        // PHY has received, and the MAC's Receive already put the device to sleep
        break;
      case EndDeviceLoraPhy::STANDBY:
        {
          NS_ASSERT_MSG (m_devClass == CLASS_A, "Class C is not allowed to switch to standby");
          break;
        }
      }
    }

    void
    LoRaMacEndDevice::OpenSecondReceiveWindow(void)
    {
      NS_LOG_FUNCTION_NOARGS();

      NS_LOG_DEBUG ("Open Second Receive Window.");

      // Check for receiver status: if it's locked on a packet, if it is class C
      //don't open this window at all.
      if (m_phy->GetObject<EndDeviceLoraPhy> ()->IsReceiving ())
      {
        NS_LOG_INFO("Won't open second receive window since we are in RX mode. Class A");

        return;
      }

      // Switch to appropriate channel and data rate
      NS_LOG_INFO("Using parameters: " << m_secondReceiveWindowFrequency << "Hz, DR"
                                       << unsigned(m_secondReceiveWindowDataRate));

      m_phy->GetObject<EndDeviceLoraPhy>()->SetFrequency(m_secondReceiveWindowFrequency);
      m_phy->GetObject<EndDeviceLoraPhy>()->SetSpreadingFactor(GetSfFromDataRate(m_secondReceiveWindowDataRate));

      // Set Phy in Rx mode
      m_phy->GetObject<EndDeviceLoraPhy>()->SwitchToRx();

      //Calculate the duration of a single symbol for the second receive window DR
      double tSym = pow(2, GetSfFromDataRate(GetSecondReceiveWindowDataRate())) / GetBandwidthFromDataRate(GetSecondReceiveWindowDataRate());
      tSym = tSym*2;

      // Schedule return to sleep after "at least the time required by the end
      // device's radio transceiver to effectively detect a downlink preamble"
      // (LoraWAN specification)
      m_closeSecondWindow = Simulator::Schedule(Seconds(m_receiveWindowDurationInSymbols * tSym),
                                                &LoRaMacEndDevice::CloseSecondReceiveWindow, this);
    }

    void
    LoRaMacEndDevice::CloseSecondReceiveWindow(void)
    {
      NS_LOG_FUNCTION_NOARGS();

      NS_LOG_DEBUG ("Close Second Receive Window.");

      Ptr<EndDeviceLoraPhy> phy = m_phy->GetObject<EndDeviceLoraPhy>();

      // - RX -> We have received a preamble.
      // - STANDBY -> Nothing was detected.
      switch (phy->GetState())
      {
      case EndDeviceLoraPhy::TX:
        break;
      case EndDeviceLoraPhy::SLEEP:
        break;
      case EndDeviceLoraPhy::RX:
        // if PHY is receiving: let it finish
        // switch to sleep otherwise
        if (phy->IsReceiving ())
        {  
          NS_LOG_DEBUG("PHY is receiving: Receive will handle the result.");
          return;
        }
        phy->SwitchToSleep ();
        break;
      case EndDeviceLoraPhy::STANDBY:
        // Turn PHY layer to sleep
        phy->SwitchToSleep();
        break;
      }

      if (m_retxParams.waitingAck)
      {
        NS_LOG_DEBUG("Class A No reception initiated by PHY: rescheduling transmission.");
        if (m_retxParams.retxLeft > 0)
        {
          NS_LOG_INFO("We have " << unsigned(m_retxParams.retxLeft) << " retransmissions left: rescheduling transmission.");
          this->Send(m_retxParams.packet);
        }
        else if (m_retxParams.retxLeft == 0 && m_phy->GetObject<EndDeviceLoraPhy>()->GetState() != EndDeviceLoraPhy::RX)
        {
          uint8_t txs = m_maxNumbTx - (m_retxParams.retxLeft);
          m_requiredTxCallback(txs, false, m_retxParams.firstAttempt, m_retxParams.packet);
          NS_LOG_DEBUG("Failure: no more retransmissions left. Used " << unsigned(txs) << " transmissions.");

          // Reset retransmission parameters
          resetRetransmissionParameters();
        }

        else
        {
          NS_ABORT_MSG("The number of retransmissions left is negative ! ");
        }
      }
      else
      {
        uint8_t txs = m_maxNumbTx - (m_retxParams.retxLeft);
        m_requiredTxCallback(txs, true, m_retxParams.firstAttempt, m_retxParams.packet);
        NS_LOG_INFO("We have " << unsigned(m_retxParams.retxLeft) << " transmissions left. We were not transmitting confirmed messages.");

        // Reset retransmission parameters
        resetRetransmissionParameters();
      }
    }

    /////////////////////////
    // Getters and Setters //
    /////////////////////////

    Time
    LoRaMacEndDevice::GetNextClassTransmissionDelay(Time waitingTime)
    {
      NS_LOG_FUNCTION_NOARGS();

      // This is a new packet from APP; it can not be sent until the end of the
      // second receive window (if the second recieve window has not closed yet)
      if (!m_retxParams.waitingAck)
      {
        if (!m_closeFirstWindow.IsExpired() ||
            !m_closeSecondWindow.IsExpired() ||
            !m_secondReceiveWindow.IsExpired())
        {
          std::cout<<"here shit"<<std::endl;
          NS_LOG_WARN("Attempting to send when there are receive windows:"
                      << " Transmission postponed.");
          // Compute the duration of a single symbol for the second receive window DR
          double tSym = pow(2, GetSfFromDataRate(GetSecondReceiveWindowDataRate())) / GetBandwidthFromDataRate(GetSecondReceiveWindowDataRate());
          // Compute the closing time of the second receive window
          Time endSecondRxWindow = Time(m_secondReceiveWindow.GetTs()) + Seconds(m_receiveWindowDurationInSymbols * tSym);

          NS_LOG_DEBUG("Duration until endSecondRxWindow for new transmission:" << (endSecondRxWindow - Simulator::Now()).GetSeconds());
          waitingTime = std::max(waitingTime, endSecondRxWindow - Simulator::Now());
        }
      }
      // This is a retransmitted packet, it can not be sent until the end of
      // ACK_TIMEOUT (this timer starts when the second receive window was open)
      else
      {
        double ack_timeout = m_uniformRV->GetValue(1, 3);
        // Compute the duration until ACK_TIMEOUT (It may be a negative number, but it doesn't matter.)
        Time retransmitWaitingTime = Time(m_secondReceiveWindow.GetTs()) - Simulator::Now() + Seconds(ack_timeout);

        NS_LOG_DEBUG("ack_timeout:" << ack_timeout << " retransmitWaitingTime:" << retransmitWaitingTime.GetSeconds());
        waitingTime = std::max(waitingTime, retransmitWaitingTime);
      }

      return waitingTime;
    }

    uint8_t
    LoRaMacEndDevice::GetFirstReceiveWindowDataRate(void)
    {
      return m_replyDataRateMatrix.at(m_dataRate).at(m_rx1DrOffset);
    }

    void
    LoRaMacEndDevice::SetSecondReceiveWindowDataRate(uint8_t dataRate)
    {
      m_secondReceiveWindowDataRate = dataRate;
    }

    uint8_t
    LoRaMacEndDevice::GetSecondReceiveWindowDataRate(void)
    {
      return m_secondReceiveWindowDataRate;
    }

    void
    LoRaMacEndDevice::SetSecondReceiveWindowFrequency(double frequencyMHz)
    {
      m_secondReceiveWindowFrequency = frequencyMHz;
    }

    double
    LoRaMacEndDevice::GetSecondReceiveWindowFrequency(void)
    {
      return m_secondReceiveWindowFrequency;
    }

    void
    LoRaMacEndDevice::SetConfirmDeviceClass(bool conf)
    {
      m_devClassConf = conf;
    }

    bool
    LoRaMacEndDevice::GetConfirmDeviceClass(void)
    {
      return m_devClassConf;
    }

    /////////////////////////
    // MAC command methods //
    /////////////////////////

    void
    LoRaMacEndDevice::OnRxClassParamSetupReq(Ptr<RxParamSetupReq> rxParamSetupReq)
    {
      NS_LOG_FUNCTION(this << rxParamSetupReq);

      bool offsetOk = true;
      bool dataRateOk = true;

      uint8_t rx1DrOffset = rxParamSetupReq->GetRx1DrOffset();
      uint8_t rx2DataRate = rxParamSetupReq->GetRx2DataRate();
      double frequency = rxParamSetupReq->GetFrequency();

      NS_LOG_FUNCTION(this << unsigned(rx1DrOffset) << unsigned(rx2DataRate) << frequency);

      // Check that the desired offset is valid
      if (!(0 <= rx1DrOffset && rx1DrOffset <= 5))
      {
        offsetOk = false;
      }

      // Check that the desired data rate is valid
      if (GetSfFromDataRate(rx2DataRate) == 0 || GetBandwidthFromDataRate(rx2DataRate) == 0)
      {
        dataRateOk = false;
      }

      // For now, don't check for validity of frequency
      m_secondReceiveWindowDataRate = rx2DataRate;
      m_rx1DrOffset = rx1DrOffset;
      m_secondReceiveWindowFrequency = frequency;

      // Craft a RxParamSetupAns as response
      NS_LOG_INFO("Adding RxParamSetupAns reply");
      m_macCommandList.push_back(CreateObject<RxParamSetupAns>(offsetOk,
                                                               dataRateOk, true));
    }

    void 
    LoRaMacEndDevice::ChangePhyStateEnd (void)
    {
      NS_LOG_FUNCTION (this);

      switch (m_devClass)
      {
      case CLASS_A:
        m_phy->GetObject<EndDeviceLoraPhy> ()->SwitchToStandby ();
        break;
      case CLASS_C:
        NS_LOG_INFO("Using parameters: " << m_secondReceiveWindowFrequency << "Hz, DR"
                                          << unsigned(m_secondReceiveWindowDataRate));
        m_phy->GetObject<EndDeviceLoraPhy>()->SetFrequency(m_secondReceiveWindowFrequency);
        m_phy->GetObject<EndDeviceLoraPhy>()->SetSpreadingFactor(GetSfFromDataRate(m_secondReceiveWindowDataRate));

        // Turn PHY layer to standby if any packet arrives from server
        m_phy->GetObject<EndDeviceLoraPhy> ()->SwitchToRx ();
        break;
      default:
        m_phy->GetObject<EndDeviceLoraPhy> ()->SwitchToStandby ();
        break;
      }
    }

    void 
    LoRaMacEndDevice::ConfirmedDownAck (void)
    {
      NS_LOG_FUNCTION (this);

      Ptr<Packet> packet = Create<Packet> (1);

      ConfDownAckTag ackTag;
      packet->RemovePacketTag (ackTag);
      packet->AddPacketTag (ackTag);
      Send (packet);


    }
  } // namespace lorawan
} // namespace ns3
