
#include "ns3/jit-queue.h"
#include "ns3/lora-tag.h"
#include "ns3/simulator.h"

#include <cmath>

namespace ns3 {
namespace lorawan {

NS_OBJECT_ENSURE_REGISTERED (JitQueue);

NS_LOG_COMPONENT_DEFINE ("JitQueue");

TypeId
JitQueue::GetTypeId (void)
{
    static TypeId tid = TypeId ("ns3::JitQueue")
            .SetParent<Queue<Packet> > ()
            .SetGroupName ("lorawan")
            .AddConstructor<JitQueue> ()
            .AddTraceSource ("CollisionDrop",
                             "Drop incoming packet due to collision.",
                             MakeTraceSourceAccessor (&JitQueue::m_traceCollisionDrop),
                             "ns3::Packet::TracedCallback")
            .AddTraceSource ("LateArrivalDrop",
                             "Drop packet that has arrived late",
                             MakeTraceSourceAccessor (&JitQueue::m_traceLateArrivalDrop),
                             "ns3::Packet::TracedCallback")
            .AddTraceSource ("TxTimePassedDrop",
                             "Trace when a packet drops due to"
                             "passing its scheduled time.",
                             MakeTraceSourceAccessor (&JitQueue::m_traceTxTimePassDrop),
                             "ns3::Packet::TracedCallback")
    ;
    
    return tid;
}

JitQueue::JitQueue () :
      Queue<Packet> (),
      NS_LOG_TEMPLATE_DEFINE ("JitQueue")
{
  NS_LOG_FUNCTION (this);
}

JitQueue::~JitQueue ()
{
  NS_LOG_FUNCTION (this);
}

bool
JitQueue::Enqueue (Ptr<Packet> pkt)
{
  NS_LOG_FUNCTION (this << pkt);

  if (GetCurrentSize () + pkt > GetMaxSize ())
    {
      NS_LOG_DEBUG ("Queue full -- dropping pkt");
      DropBeforeEnqueue (pkt);
      return false;
    }

  LoraTag pktTag;
  NS_ASSERT_MSG (pkt->PeekPacketTag (pktTag), "It is not LoRaWAN packet.");


  if ((pktTag.GetTxDownlinkSchedule () - Simulator::Now ())
            .GetSeconds () < LATENCY_TSH)
    {
      NS_LOG_DEBUG ("Packet arrived with latency. It will will not get enqueue.");

      m_traceLateArrivalDrop (pkt);
      return false;
    }
  
  if (this->IsEmpty ())
    {
      NS_LOG_DEBUG ("Queue is empty. New packet will push in.");

      return DoEnqueue (begin (), pkt);
    }

  Time pktTime = pktTag.GetTxDownlinkSchedule ();
  for (auto it = begin (); it != end (); ++it)
    {
      LoraTag itTag;
      (*it)->PeekPacketTag (itTag);

      Time itTime = itTag.GetTxDownlinkSchedule ();

      if (std::abs (pktTime.GetSeconds () - itTime.GetSeconds ()) < COLLIDE_TSH)
        {
          NS_LOG_DEBUG ("Packet collides with a packet already in queue.");

          m_traceCollisionDrop (pkt);
          return false;
        }

      if (itTime > pktTime)
        {
          NS_LOG_DEBUG ("Packet will be inserted inorder.");

          return DoEnqueue (it, pkt);
        }
    }

  NS_LOG_DEBUG ("Packet must push at the end of queue");
  return DoEnqueue (end (), pkt);
}

Ptr<Packet>
JitQueue::Dequeue (void)
{
  NS_LOG_FUNCTION (this);

  if (this->IsEmpty ())
    {
      NS_LOG_DEBUG ("Queue is empty ....... no packet available.");
      return 0;
    }
  LoraTag pktTag;
  Ptr<Packet> pkt = DoDequeue (begin ());
  pkt->PeekPacketTag (pktTag);

  Time nowTime = Simulator::Now ();

  while (pktTag.GetTxDownlinkSchedule () <= nowTime)
    {
      NS_LOG_DEBUG ("Packet will drop out of queue"<< 
                        "because tx time is passed.");
      m_traceTxTimePassDrop (pkt);

      if (this->IsEmpty ())
        {
          NS_LOG_DEBUG ("Queue is empty ....... no packet available.");
          return 0;
        }

      pkt = DoDequeue (begin ());
      pkt->PeekPacketTag(pktTag);
    }

  NS_LOG_LOGIC ("Popped " << pkt);

  return pkt;
}

Ptr<const Packet>
JitQueue::Peek (void) const
{
  NS_LOG_FUNCTION (this);

  return DoPeek (begin ());
}

Ptr<const Packet>
JitQueue::JitPeek (void)
{
  LoraTag pktTag;
  Ptr<const Packet> pkt = DoPeek (begin ());
  pkt->PeekPacketTag (pktTag);

  Time nowTime = Simulator::Now ();

  while (pktTag.GetTxDownlinkSchedule () <= nowTime)
    {
      NS_LOG_DEBUG ("Packet will drop out of queue"<< 
                        "because tx time is passed.");
      m_traceTxTimePassDrop (pkt);

      DoDequeue (begin ());
      if (this->IsEmpty ())
        {
          NS_LOG_DEBUG ("Queue is empty ....... no packet available.");
          return 0;
        }

      pkt = DoPeek (begin ());
      pkt->PeekPacketTag(pktTag);
    }


  return pkt;
}

Ptr<Packet>
JitQueue::Remove (void)
{
  NS_LOG_FUNCTION (this);

  Ptr<Packet> pkt = DoRemove (begin ());

  NS_LOG_LOGIC ("Removed " << pkt);

  return pkt;
}

Ptr<Packet>
JitQueue::Remove (ConstIterator pos)
{
  NS_LOG_FUNCTION (this);

  Ptr<Packet> pkt = DoRemove (pos);

  NS_LOG_LOGIC ("Removed " << pkt);

  return pkt;
}

} // namespace lorawan
} // namespace ns3