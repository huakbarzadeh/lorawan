
#ifndef JIT_QUEUE_H
#define JIT_QUEUE_H

#include "ns3/object.h"
#include "ns3/queue.h"
#include "ns3/packet.h"
#include "ns3/traced-callback.h"

#define COLLIDE_TSH 0.2 //<! collision threshold
#define LATENCY_TSH 0.1 //<! latency threshold

namespace ns3 {
namespace lorawan {

class JitQueue : public Queue<Packet>
{
public:
  static TypeId GetTypeId (void);

  JitQueue ();
  virtual ~JitQueue ();

  virtual bool Enqueue (Ptr<Packet>);
  virtual Ptr<Packet> Dequeue (void);
  virtual Ptr<const Packet> Peek (void) const;
  virtual Ptr<Packet> Remove (void);
  virtual Ptr<Packet> Remove (ConstIterator);
  virtual Ptr<const Packet> JitPeek (void); //!< This Peek will iterate through queue
                                            //!< and peek first item which it's timestamp
                                            //!< is not passed and drop the rest
  

private:
  using Queue<Packet>::begin;
  using Queue<Packet>::end;
  using Queue<Packet>::DoEnqueue;
  using Queue<Packet>::DoDequeue;
  using Queue<Packet>::DoRemove;
  using Queue<Packet>::DoPeek;
  
  NS_LOG_TEMPLATE_DECLARE;

  /// Traced callback: fired when a packet is droped before entering queu
  ///                   due to collision with other packets in queue
  TracedCallback<Ptr<const Packet> > m_traceCollisionDrop;
  /// Traced callback: fired when a packet arrives late
  TracedCallback<Ptr<const Packet> > m_traceLateArrivalDrop;
  /// Traced callback: fired when a packet's tx time is passed, 
  ///                   and it will be droped.
  TracedCallback<Ptr<const Packet> > m_traceTxTimePassDrop;
};

} // namespace lorawan
} // namespace ns3



#endif  /* JIT_QUEUE_H */