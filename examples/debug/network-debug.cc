

#include "ns3/simulator.h"
#include "ns3/log.h"
#include "ns3/pointer.h"
#include "ns3/constant-position-mobility-model.h"
#include "ns3/lora-helper.h"
#include "ns3/node-container.h"
#include "ns3/mobility-helper.h"
#include "ns3/position-allocator.h"
#include "ns3/double.h"
#include "ns3/random-variable-stream.h"
//#include "ns3/periodic-sender-helper.h"
#include "ns3/one-shot-sender-helper.h"
#include "ns3/command-line.h"
#include "ns3/network-server-helper.h"
#include "ns3/correlated-shadowing-propagation-loss-model.h"
#include "ns3/building-penetration-loss.h"
#include "ns3/building-allocator.h"
#include "ns3/buildings-helper.h"
#include "ns3/forwarder-helper.h"
#include "ns3/event-driven-app-helper.h"

/* Energy model and source */
#include "ns3/li-ion-energy-source-helper.h"
#include "ns3/loramac-radio-energy-model-helper.h"

/* LoRa modules */
#include "ns3/end-device-lora-phy.h"
#include "ns3/gateway-lora-phy.h"
#include "ns3/end-device-lorawan-mac.h"
#include "ns3/gateway-lorawan-mac.h"

#include <algorithm>
#include <ctime>

using namespace ns3;
using namespace lorawan;

NS_LOG_COMPONENT_DEFINE("NetworkDebug");

// Network settings
int nDevices = 1;
int nGateways = 1;
double radius = 7500;
double simulationTime = 1200;

bool realisticChannelModel = true;

int appPeriodSeconds = 600;

// Class C server frame timeout
double cFrameTimeOut = 10.0;

// Output control
bool print = true;

void PhyRxDrop (Ptr<const Packet> ptr)
{
    std::cout<<"Packet droped."<<std::endl;
}

void PhyTxDrop (Ptr<const Packet> ptr)
{
    std::cout<<"Packet droped."<<std::endl;
}

int main(int argc, char *argv[])
{
    uint32_t tmp = 1;

    CommandLine cmd;
    cmd.AddValue("nTest", "Just a number for test", tmp);
    cmd.AddValue("cFrameTimeOut", "Class C server frame timeout", cFrameTimeOut);
    cmd.Parse(argc, argv);

    // LogComponentEnable("SimpleTestEnv", LOG_LEVEL_ALL);
    // LogComponentEnable("PeriodicSender", LOG_LEVEL_ALL);
    // LogComponentEnable("LorawanMacHelper", LOG_LEVEL_ALL);

    // LogComponentEnableAll(LOG_LEVEL_FUNCTION);
    // LogComponentEnable("EndDeviceLorawanMac", LOG_LEVEL_INFO);
    // LogComponentEnable("SimpleEndDeviceLoraPhy", LOG_LEVEL_INFO);
    // LogComponentEnable("LoraChannel", LOG_LEVEL_INFO);
    // LogComponentEnable("EndDeviceLorawanMac", LogLevel (LOG_PREFIX_FUNC | LOG_PREFIX_NODE | LOG_LEVEL_DEBUG));
    // LogComponentEnable("Packet", LOG_LEVEL_INFO );
    // LogComponentEnable("BatterySourceModel", LOG_LEVEL_FUNCTION);
    // LogComponentEnable("BatterySourceModel", LOG_LEVEL_INFO);
    // LogComponentEnable("LoRaMacRadioEnergyModel", LogLevel (LOG_PREFIX_FUNC | LOG_PREFIX_NODE | LOG_LEVEL_DEBUG));
    // LogComponentEnable ("EndDeviceLorawanMac",LOG_LEVEL_DEBUG);
    // LogComponentEnable ("LoRaMacEndDevice", LOG_LEVEL_FUNCTION);
    // LogComponentEnable ("SimpleEndDeviceLoraPhy", LOG_LEVEL_DEBUG);
    // LogComponentEnable ("EndDeviceLorawanMac", LOG_LEVEL_DEBUG);
    // LogComponentEnable ("SimpleEndDeviceLoraPhy", LOG_LEVEL_FUNCTION);
    //Gateway log components
    // LogComponentEnable ("SimpleGatewayLoraPhy", LOG_LEVEL_FUNCTION);
    // LogComponentEnable ("GatewayLorawanMac", LOG_LEVEL_FUNCTION);
    // LogComponentEnable ("SimpleGatewayLoraPhy", LOG_LEVEL_DEBUG);
    LogComponentEnable("Forwarder", LogLevel (LOG_PREFIX_FUNC | LOG_PREFIX_TIME | LOG_LEVEL_DEBUG));
    
    //Server side log components
    LogComponentEnable ("NetworkClassCManageApp", LogLevel (LOG_PREFIX_FUNC | LOG_PREFIX_TIME | LOG_LEVEL_DEBUG));
    LogComponentEnable ("EndDeviceStatus", LogLevel (LOG_PREFIX_FUNC | LOG_PREFIX_TIME | LOG_LEVEL_DEBUG));
    // LogComponentEnable ("NetworkScheduler", LogLevel (LOG_PREFIX_FUNC | LOG_PREFIX_NODE | LOG_LEVEL_DEBUG));
    // LogComponentEnable ("NetworkStatus", LOG_LEVEL_DEBUG);
    // LogComponentEnable ("NetworkController", LOG_LEVEL_DEBUG);
    // LogComponentEnable ("NetworkServer", LOG_LEVEL_DEBUG);
    // LogComponentEnable ("NetworkControllerComponent", LOG_LEVEL_DEBUG);
    // LogComponentEnable ("GatewayStatus", LOG_LEVEL_DEBUG);



    NS_LOG_INFO("Here is test number : "<<tmp);
    NS_LOG_INFO("Program begun!");

    // Time period for devices Send intervals
    Time appPeriod = Seconds (appPeriodSeconds);
    
    // Mobility
    MobilityHelper mobility;
    mobility.SetPositionAllocator ("ns3::UniformDiscPositionAllocator", "rho", DoubleValue (radius),
                                    "X", DoubleValue (0.0), "Y", DoubleValue (0.0));
    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");

    NS_LOG_INFO("Mobility OK");

    /************************
     *  Create the channel  *
     ************************/

    // Create the lora channel object
    Ptr<LogDistancePropagationLossModel> loss = CreateObject<LogDistancePropagationLossModel> ();
    loss->SetPathLossExponent (3.76);
    loss->SetReference (1, 7.7);

    if (realisticChannelModel)
        {
        // Create the correlated shadowing component
        Ptr<CorrelatedShadowingPropagationLossModel> shadowing =
            CreateObject<CorrelatedShadowingPropagationLossModel> ();

        // Aggregate shadowing to the logdistance loss
        loss->SetNext (shadowing);

        // Add the effect to the channel propagation loss
        Ptr<BuildingPenetrationLoss> buildingLoss = CreateObject<BuildingPenetrationLoss> ();

        shadowing->SetNext (buildingLoss);
        }

    Ptr<PropagationDelayModel> delay = CreateObject<ConstantSpeedPropagationDelayModel> ();

    Ptr<LoraChannel> channel = CreateObject<LoraChannel> (loss, delay);

    NS_LOG_INFO("Channel Ok.");

    /************************
     *  Create the helpers  *
     ************************/

    // Create the LoraPhyHelper
    LoraPhyHelper phyHelper = LoraPhyHelper ();
    phyHelper.SetChannel (channel);

    // Create the LorawanMacHelper
    //LoRaMacHelper macHelper = LoRaMacHelper ();
    LorawanMacHelper macHelper = LorawanMacHelper();

    // Create the LoraHelper
    LoraHelper helper = LoraHelper ();
    helper.EnablePacketTracking (); // Output filename
    // helper.EnableSimulationTimePrinting ();

    //Create the NetworkServerHelper
    NetworkServerHelper nsHelper = NetworkServerHelper ();

    //Create the ForwarderHelper
    ForwarderHelper forHelper = ForwarderHelper ();

    NS_LOG_INFO("Helpers OK.");

    /************************
     *  Create End Devices Class A  *
     ************************/

    // Create a set of nodes
    NodeContainer endDevices;
    endDevices.Create (nDevices);

    // Assign a mobility model to each node
    mobility.Install (endDevices);

    // Make it so that nodes are at a certain height > 0
    for (NodeContainer::Iterator j = endDevices.Begin (); j != endDevices.End (); ++j)
        {
        Ptr<MobilityModel> mobility = (*j)->GetObject<MobilityModel> ();
        Vector position = mobility->GetPosition ();
        position.z =3.5;
        mobility->SetPosition (position);
        }

    // Create the LoraNetDevices of the end devices
    uint8_t nwkId = 54;
    uint32_t nwkAddr = 1864;
    Ptr<LoraDeviceAddressGenerator> addrGen =
        CreateObject<LoraDeviceAddressGenerator> (nwkId, nwkAddr);

    // Create the LoraNetDevices of the end devices
    macHelper.SetAddressGenerator (addrGen);
    phyHelper.SetDeviceType (LoraPhyHelper::ED);
    macHelper.SetDeviceType(LorawanMacHelper::ED_C);
    macHelper.Set ("MType", StringValue ("Unconfirmed"));
    // macHelper.Set ("DataRate", UintegerValue (0));
    // macHelper.Set ("MaxTransmissions", UintegerValue (8));
    NetDeviceContainer endDevicesNetDevices = helper.Install (phyHelper, macHelper, endDevices);

    // Now end devices are connected to the channel

    // Connect trace sources
    for (NodeContainer::Iterator j = endDevices.Begin (); j != endDevices.End (); ++j)
    {
        Ptr<Node> node = *j;
        Ptr<LoraNetDevice> loraNetDevice = node->GetDevice (0)->GetObject<LoraNetDevice> ();
        Ptr<LoraPhy> phy = loraNetDevice->GetPhy ();
    }

    /************************
     *  Create End Devices Class C  *
     ************************/

    // Create a set of nodes
    // NodeContainer endDevicesC;
    // endDevicesC.Create (nDevices);

    // // Assign a mobility model to each node
    // mobility.Install (endDevicesC);

    // // Make it so that nodes are at a certain height > 0
    // for (NodeContainer::Iterator j = endDevicesC.Begin (); j != endDevicesC.End (); ++j)
    //     {
    //     Ptr<MobilityModel> mobility = (*j)->GetObject<MobilityModel> ();
    //     Vector position = mobility->GetPosition ();
    //     position.z =3.5;
    //     mobility->SetPosition (position);
    //     }

    // Create the LoraNetDevices of the end devices
    // Ptr<LoraDeviceAddressGenerator> addrGenC =
    //     CreateObject<LoraDeviceAddressGenerator> (nwkId, 1870);

    // // Create the LoraNetDevices of the end devices
    // macHelper.SetAddressGenerator (addrGenC);
    // phyHelper.SetDeviceType (LoraPhyHelper::ED);
    // macHelper.SetDeviceType(LorawanMacHelper::ED_C);
    // helper.Install (phyHelper, macHelper, endDevicesC);

    // // Now end devices are connected to the channel

    // // Connect trace sources
    // for (NodeContainer::Iterator j = endDevicesC.Begin (); j != endDevicesC.End (); ++j)
    // {
    //     Ptr<Node> node = *j;
    //     Ptr<LoraNetDevice> loraNetDevice = node->GetDevice (0)->GetObject<LoraNetDevice> ();
    //     Ptr<LoraPhy> phy = loraNetDevice->GetPhy ();
    // }

    NS_LOG_INFO("End Devices OK.");
    
    /*********************
     *  Create Gateways  *
     *********************/

    // Create the gateway nodes (allocate them uniformely on the disc)
    NodeContainer gateways;
    gateways.Create (nGateways);

    Ptr<ListPositionAllocator> allocator = CreateObject<ListPositionAllocator> ();
    // Make it so that nodes are at a certain height > 0
    allocator->Add (Vector (0.0, 0.0, 15.0));
    mobility.SetPositionAllocator (allocator);
    mobility.Install (gateways);

    // Create a netdevice for each gateway
    phyHelper.SetDeviceType (LoraPhyHelper::GW);
    macHelper.SetDeviceType (LorawanMacHelper::GW);
    helper.Install (phyHelper, macHelper, gateways);

    NS_LOG_INFO("Gateways OK.");

    /**********************
   *  Handle buildings  *
   **********************/

//   double xLength = 130;
//   double deltaX = 32;
//   double yLength = 64;
//   double deltaY = 17;
//   int gridWidth = 2 * radius / (xLength + deltaX);
//   int gridHeight = 2 * radius / (yLength + deltaY);
//   if (realisticChannelModel == false)
//     {
//       gridWidth = 0;
//       gridHeight = 0;
//     }
//   Ptr<GridBuildingAllocator> gridBuildingAllocator;
//   gridBuildingAllocator = CreateObject<GridBuildingAllocator> ();
//   gridBuildingAllocator->SetAttribute ("GridWidth", UintegerValue (gridWidth));
//   gridBuildingAllocator->SetAttribute ("LengthX", DoubleValue (xLength));
//   gridBuildingAllocator->SetAttribute ("LengthY", DoubleValue (yLength));
//   gridBuildingAllocator->SetAttribute ("DeltaX", DoubleValue (deltaX));
//   gridBuildingAllocator->SetAttribute ("DeltaY", DoubleValue (deltaY));
//   gridBuildingAllocator->SetAttribute ("Height", DoubleValue (6));
//   gridBuildingAllocator->SetBuildingAttribute ("NRoomsX", UintegerValue (2));
//   gridBuildingAllocator->SetBuildingAttribute ("NRoomsY", UintegerValue (4));
//   gridBuildingAllocator->SetBuildingAttribute ("NFloors", UintegerValue (2));
//   gridBuildingAllocator->SetAttribute (
//       "MinX", DoubleValue (-gridWidth * (xLength + deltaX) / 2 + deltaX / 2));
//   gridBuildingAllocator->SetAttribute (
//       "MinY", DoubleValue (-gridHeight * (yLength + deltaY) / 2 + deltaY / 2));
//   BuildingContainer bContainer = gridBuildingAllocator->Create (gridWidth * gridHeight);

  BuildingsHelper::Install (endDevices);
//   BuildingsHelper::Install (endDevicesC);
  BuildingsHelper::Install (gateways);

  // Print the buildings
//   if (print)
//     {
//       std::ofstream myfile;
//       myfile.open ("buildings.txt");
//       std::vector<Ptr<Building>>::const_iterator it;
//       int j = 1;
//       for (it = bContainer.Begin (); it != bContainer.End (); ++it, ++j)
//         {
//           Box boundaries = (*it)->GetBoundaries ();
//           myfile << "set object " << j << " rect from " << boundaries.xMin << "," << boundaries.yMin
//                  << " to " << boundaries.xMax << "," << boundaries.yMax << std::endl;
//         }
//       myfile.close ();
//     }

    NS_LOG_INFO("Buildings OK.");

    /**********************************************
     *  Set up the end device's spreading factor  *
     **********************************************/

    macHelper.SetSpreadingFactorsUp (endDevices, gateways, channel);

    NS_LOG_INFO("Completed configuration");

    /*********************************************
     *  Install applications on the end devices  *
     *********************************************/

    Time appStopTime = Seconds (simulationTime);
    OneShotSenderHelper appHelper = OneShotSenderHelper ();
    // appHelper.SetPeriod (Seconds (appPeriodSeconds));
    // appHelper.SetPacketSize (23);
    appHelper.SetSendTime (appPeriod);
    // Ptr<RandomVariableStream> rv = CreateObjectWithAttributes<UniformRandomVariable> (
    //     "Min", DoubleValue (0), "Max", DoubleValue (10));
    ApplicationContainer appContainer = appHelper.Install (endDevices);

    appContainer.Start (Seconds (0));
    appContainer.Stop (appStopTime);

    // C devices
    // appHelper.SetSendTime (Seconds (appPeriodSeconds + 200));
    // ApplicationContainer appContainerC = appHelper.Install (endDevicesC);

    // appContainer.Start (Seconds (0));
    // appContainer.Stop (appStopTime);

    // appContainerC.Start (Seconds (0));
    // appContainerC.Stop (appStopTime);

    /************************
     *  Create Energy Model *
     ************************/
    // BatterySourceModelHelper batterySourceHelper;
    // LoRaMacRadioEnergyModelHelper radioEnergyHelper;
    // LiIonEnergySourceHelper liIonEnergySourceHelper;
    // LoRaMacRadioEnergyModelHelper radioEnergyHelper;

    // configure energy source
    // batterySourceHelper.Set ("BatterySourceModelNominalCapacity", DoubleValue (2400)); // Capacity in mAh

    // liIonEnergySourceHelper.Set ("PeriodicEnergyUpdateInterval", TimeValue (Seconds (100.0)));

    // radioEnergyHelper.Set ("StandbyCurrentmA", DoubleValue (0.0014));
    // radioEnergyHelper.Set ("TxCurrentmA", DoubleValue (0.028));
    // radioEnergyHelper.Set ("SleepCurrentmA", DoubleValue (0.0000015));
    // radioEnergyHelper.Set ("RxCurrentmA", DoubleValue (0.0112));

    // radioEnergyHelper.SetTxCurrentModel ("ns3::LoRaMacTxCurrentModel",
    //                                     "TxCurrent", DoubleValue (0.028));

    // install source on EDs' nodes
    // EnergySourceContainer sources = liIonEnergySourceHelper.Install (endDevices);

    // install device model
    // DeviceEnergyModelContainer deviceModels = radioEnergyHelper.Install
    //     (endDevicesNetDevices, sources);

    /**************************
     *  Create Network Server  *
     ***************************/

    // Create the NS node
    NodeContainer networkServer;
    networkServer.Create (1);

    // Containing all end devices
    // NodeContainer LoRaMACs;
    // LoRaMACs.Add (endDevices);
    // LoRaMACs.Add (endDevicesC);

    // Create a NS for the network
    nsHelper.SetAttribute ("ServerCFrameTimeOut", DoubleValue (cFrameTimeOut));
    nsHelper.SetEndDevices (endDevices);
    nsHelper.SetGateways (gateways);
    nsHelper.SetRxDropCallback (MakeCallback (&PhyRxDrop));
    nsHelper.SetTxDropCallback (MakeCallback (&PhyTxDrop));
    nsHelper.Install (networkServer);

    //Create a forwarder for each gateway
    forHelper.Install (gateways);

    ////////////////
    // Simulation //
    ////////////////

    Simulator::Stop (appStopTime);

    NS_LOG_INFO ("Running simulation...");
    Simulator::Run ();

    Simulator::Destroy ();

    ///////////////////////////
    // Print results to file //
    ///////////////////////////
    NS_LOG_INFO ("Computing performance metrics...");

    LoraPacketTracker &tracker = helper.GetPacketTracker ();
    std::cout << tracker.CountMacPacketsGlobally (Seconds (0), appStopTime + Hours (1)) << std::endl;


    return 0;
}